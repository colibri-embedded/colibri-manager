/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file os_helpers.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
// system
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string>
#include <array>
#include <exception>
#include <fstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
// 3rdparty
#define FMT_HEADER_ONLY
#include <fmt/format.h>
// local
#include <helper/os_helpers.hpp>
#include <helper/string_helpers.hpp>

#include <iostream>


namespace helper {
	namespace os {

/**
 * Get a list of files contained in <dir_name> directory.
 *
 * @param dir_name Directory path.
 * @returns unique pinter to std::vector<str::string>
 */
uptrStrings getDirectoryFiles(const std::string& dir_name) 
{
	uptrStrings files(new std::vector<std::string>);

    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir_name.c_str())) == NULL) {
        std::string msg = fmt::format("Cannot open {}", dir_name);
        throw std::system_error( 
        		errno, 
        		std::system_category(), 
        		msg);
    }

    while ((dirp = readdir(dp)) != NULL) {
    	std::string file_name = std::string(dirp->d_name);
    	if(file_name != "." && file_name != "..") {
    		files->push_back(file_name);
    	}
    }
    closedir(dp);

	return std::move(files);
}

/**
 * Copy all files from srcDir to dstDir
 *
 * @param   srcDir
 * @param   dstDir
 * @returns true on success
 */
bool copyFiles(const std::string& srcDir, const std::string& dstDir)
{
	std::string cmd = "cp -R ";
	cmd += pathJoin({srcDir, "/*"});
	cmd += " " + dstDir;
	return os::executeSimple(cmd);
}

/**
 * 
 *
 */
bool copyFile(const std::string& srcFile, const std::string& dstFile)
{
	std::string cmd = "cp ";
	cmd += srcFile + " ";
	cmd += dstFile;
	return os::executeSimple(cmd);
}

bool createSoftlink(const std::string& srcFile, const std::string& dstFile, bool overwrite)
{
	std::string cmd = "ln -s ";
	if(overwrite)
		cmd += "-f ";
	cmd += srcFile + " ";
	cmd += dstFile;
	return os::executeSimple(cmd);
}

/**
 * 
 *
 */
bool createFile(const std::string& filename, const std::string& content, bool append)
{
	auto flags = std::ofstream::out;
	if(append) {
		flags |=  std::ofstream::app;
	} else {
		flags |=  std::ofstream::trunc;
	}
		
	std::ofstream outfile (filename, flags);
	outfile << content;
	return outfile.good();
}

bool changeMode(const std::string& filename, unsigned mode)
{
	return chmod(filename.c_str(), mode) == 0;
}

/**
 * Join path elements and add the required separators
 *
 * @param   Individual path elements
 * @returns Path string
 */
std::string pathJoin(std::initializer_list<std::string> paths)
{
    std::string full_path;
    for(auto path: paths) {
        if(path[0] != '/' and full_path[full_path.length()-1] != '/')
            full_path += '/';
        full_path += path;
    }
    return full_path;
}

/**
 * Return only the basename part of the provided filename.
 *
 * @param filename Full filename
 * @return basename
 */
std::string baseName(const std::string& filename)
{
    auto segments = helper::str::split(filename, '/');
    if(segments.size() > 0) {
        return segments[segments.size()-1];
    }
    return "";
}

/**
 * Return only the directory part of the provided filename.
 *
 * @param filename Full filename
 * @return Directory the file is located in
 */
std::string dirName(const std::string& filename)
{
    auto found = filename.find_last_of('/');
    if(found == std::string::npos) {
        return filename;
    }
    return filename.substr(0, found);
}

/**
 * Return only the file extension.
 * 
 * @param filename Full filename
 * @return File extension including the dot or an emptry string
 */
std::string fileExtension(const std::string& filename)
{
	if (filename.find_last_of(".") != std::string::npos)
		return filename.substr(filename.find_last_of(".") );
	return "";
}

/**
 * Check if the file or directory exitsts.
 *
 */
bool exists(const std::string& path)
{
    struct stat sb;
    if (stat(path.c_str(), &sb) == -1) {
        return false;
    }
    return true;
}

bool isDirectory(const std::string& path)
{
    struct stat sb;
    if (stat(path.c_str(), &sb) != -1) {
		if(S_ISDIR(sb.st_mode)) {
			return true;
		}
    }
    return false;
}

bool isFile(const std::string& path)
{
    struct stat sb;
    if (stat(path.c_str(), &sb) != -1) {
		if(S_ISREG(sb.st_mode)) {
			return true;
		}
    }
    return false;
}

bool isExecutable(const std::string& path)
{
    struct stat sb;
    if (stat(path.c_str(), &sb) != -1) {
		if(S_ISREG(sb.st_mode) and (sb.st_mode & S_IXUSR) ) {
			return true;
		}
    }
	return false;
}

/**
 * Remove a file.
 *
 */
bool remove(const std::string& filename)
{
	std::string cmd = "rm -f ";
	cmd += filename;
    return executeSimple(cmd);
}

/**
 * Remove a directory and all the files in it.
 *
 */
bool removeDirectory(const std::string& path)
{
	std::string cmd = "rm -rf ";
	cmd += path;
	return executeSimple(cmd);
}

/**
 * Make a directory.
 * 
 * @param dirname Directory name or full path
 * @param recursive Create all missing parent directories
 * @param mode Directory permission
 */
bool mkDir(const std::string& dirname, bool recursive, unsigned mode)
{
    struct stat sb;
    auto dirs = helper::str::split(dirname, '/');
    std::string path;
    for(auto &dir : dirs) {
        path += "/" + dir;

        if (stat(path.c_str(), &sb) != -1) {
            if (!S_ISDIR (sb.st_mode)) {
                return false;
            }
        }

		if( exists(path) ) {
			if(not recursive) {
				return false;
			}
		} else {
            if( mkdir(path.c_str(), mode) < 0 ) {
                return false;
            }
		}
    }

    return true;
}

/**
 * Execute a shell command.
 * 
 */
std::string execute(const std::string& command, unsigned *exitCode)
{
	std::array<char, 400> buffer;
	std::string result;
	FILE *pipe = popen(command.c_str(), "r");

	if(!pipe)
		throw std::runtime_error(command + " failed!");

	while (!feof(pipe)) {
		if (fgets(buffer.data(), 128, pipe) != nullptr)
		result += buffer.data();
	}

    auto returnCode = pclose(pipe);

	if(exitCode != nullptr) {
    	if(WIFEXITED(returnCode)) {
    		*exitCode = WEXITSTATUS(returnCode);
    	}
	}
	
	// std::cout << "EXEC: " << command << std::endl;
	// std::cout << "  >> " << result << std::endl;

	return result;
}

bool executeSimple(const std::string& command, unsigned successExitCode)
{
	unsigned exitCode = -1;
	execute(command, &exitCode);
	return exitCode == successExitCode;
}

}}
