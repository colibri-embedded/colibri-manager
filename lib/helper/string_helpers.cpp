/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file string_helpers.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#include <helper/string_helpers.hpp>
#include <sstream>
#include <iterator>
#include <algorithm> 

std::vector<std::string> helper::str::split(std::string const &in, char sep) {
    std::string::size_type b = 0;
    std::vector<std::string> result;

    while ((b = in.find_first_not_of(sep, b)) != std::string::npos) {
        auto e = in.find_first_of(sep, b);
        result.push_back(in.substr(b, e-b));
        b = e;
    }
    return result;
}

std::string helper::str::join(const std::vector<std::string> &elements, const std::string& separator, const std::string& prefix, const std::string& suffix)
{
	std::string result;

	for(auto it = elements.begin(); it != elements.end(); ++it) {
	    if(std::next(it) == elements.end()) {
	        result += prefix + *it + suffix;
	    } else {
	    	result += prefix + *it + suffix + separator;
	    }
	}

	return result;
}

void helper::str::ltrim_ref(std::string &s) 
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
}

void helper::str::rtrim_ref(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

void helper::str::trim_ref(std::string &s)
{
	ltrim_ref(s);
	rtrim_ref(s);
}

std::string helper::str::rtrim(std::string s)
{
	rtrim_ref(s);
	return s;
}

std::string helper::str::ltrim(std::string s)
{
	ltrim_ref(s);
	return s;
}

std::string helper::str::trim(std::string s)
{
	trim_ref(s);
	return s;
}