/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file http_client.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#include <network/http_client.hpp>
#include <iostream>
#include <fstream>
#include <regex>

using namespace ce::network;

struct WriteThis {
	const char *readptr;
	int sizeleft;
};

int progress_callback_fn(void *clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow) 
{
	auto client = (HttpClient*)clientp;
	
	if(client->progressCallback != nullptr) {
		client->progressCallback(dltotal, dlnow);
	}
	
	return 0;
}

size_t write_to_string_callback(void *ptr, size_t size, size_t nmemb, std::string* data) 
{
	data->append((char*) ptr, size * nmemb);
	return size * nmemb;
}

size_t write_to_file_callback(void *ptr, size_t size, size_t nmemb, std::ofstream* file) 
{
	size_t written = size * nmemb; //fwrite(ptr, size, nmemb, stream);
	file->write((const char*)ptr, written);
	return written;
}

static size_t readCallback(void *ptr, size_t size, size_t nmemb, void *userp)
{
	struct WriteThis *pooh = (struct WriteThis *)userp;

	if(size*nmemb < pooh->sizeleft) {
		*(char *)ptr = pooh->readptr[0]; /* copy one single byte */ 
		pooh->readptr++;                 /* advance pointer */ 
		pooh->sizeleft--;                /* less data left */ 
		return 1;                        /* we return 1 byte at a time! */ 
	}

	return 0;                          /* no more data left to deliver */ 
}

HttpClient::HttpClient(const std::string& url) 
{
	setBaseUrl(url);
	progressCallback = nullptr;
	finishedCallback = nullptr;
}

void HttpClient::setBaseUrl(const std::string& url)
{
	if( url.length() > 0) {
		base_url = url;
	}
}

std::string HttpClient::gql_query(const std::string& suburl, const std::string& data) 
{
	CURL *curl;
	curl = curl_easy_init();
	if(curl) {
		std::string url = base_url + suburl;
		std::string response_body;
		long response_code;

		struct curl_slist *headers = NULL;
		headers = ::curl_slist_append(headers, "Accept: application/json");
		headers = ::curl_slist_append(headers, "Content-Type: application/json");
		headers = ::curl_slist_append(headers, "charsets: utf-8");

		struct WriteThis post_data;

		post_data.readptr = data.c_str();
		post_data.sizeleft = data.length();

		::curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		::curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string_callback);
		::curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_body);
		::curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
		::curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
		::curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

		// POST
		//curl_easy_setopt(curl, CURLOPT_POST, 1L);

		std::string query_data = "{ \"query\": \""+ data;
		query_data +=  "\"}";

		::curl_easy_setopt(curl, CURLOPT_POSTFIELDS, query_data.c_str() );
		// curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		::curl_easy_setopt(curl, CURLOPT_READFUNCTION, readCallback);
		::curl_easy_setopt(curl, CURLOPT_READDATA, &post_data);

		::curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
		::curl_easy_perform(curl);
		::curl_easy_cleanup(curl);

		// std::cout << "code: \n" << response_code << std::endl;
		// std::cout << "body: \n" << response_body << std::endl;
		// std::cout << "header: \n" << header_string << std::endl;

		return response_body;
	} else {
		std::cout << "error reading gql query" << std::endl;
		return "";
	}
}

bool HttpClient::downloadFile(const std::string& sub_url, const std::string& fileName)
{
	CURL *curl;
	curl = curl_easy_init();
	if(curl) {
		std::ofstream outfile (fileName, std::ofstream::binary);
		long response_code;
		std::string url = base_url + sub_url;

		::curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		::curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_file_callback);
		::curl_easy_setopt(curl, CURLOPT_WRITEDATA, &outfile);
		::curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
		::curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
		
		::curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_callback_fn);
		::curl_easy_setopt(curl, CURLOPT_XFERINFODATA, this);
		::curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);

		int res = ::curl_easy_perform(curl);
		::curl_easy_cleanup(curl);
		
		if(finishedCallback != nullptr) {
			finishedCallback();
		}
		
		if(res == CURLE_OK) {
			return true;
		}
	}
	return false;
}
