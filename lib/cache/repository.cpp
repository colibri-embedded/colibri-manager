/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file repository.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#include <cache/repository.hpp>

using namespace ce::cache;

void Repository::addItem(Item* item) 
{
	items.push_back( sptrItem(item) );
	item->repo = this;
}

std::string Repository::toString() const
{
	std::string str;
	str += name + " @" + type;
	return str;
}

json Repository::toJson()
{
	json data;
	data["name"] = name;
	data["type"] = type;
	data["url"] = url;
	data["items"] = json::array();
	data["branches"] = json::array();

	for(auto &item : items) {
		data["items"].push_back( item->toJson() );
	}

	for(auto &branch : branches) {
		data["branches"].push_back( branch );
	}

	return data;
}

bool Repository::fromJson(json &obj)
{
	if( obj["name"] != nullptr )
		name = obj["name"];

	if( obj["type"] != nullptr )
		type = obj["type"];

	if( obj["url"] != nullptr )
		url = obj["url"];

	if( obj["items"] != nullptr ) {
		for(auto &item: obj["items"]) {
			auto new_item = new Item();
			if( new_item->fromJson(item) ) {
				addItem(new_item);
			} else {
				delete new_item;
			}
		}
	}

	if( obj["branches"] != nullptr ) {
		for(auto &branch: obj["branches"]) {
			branches.push_back(branch);
		}
	}

	return true;
}