/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file version.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#include <cache/version.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>

#undef major
#undef minor

using namespace ce::cache;

Version::Version(const std::string& version) 
{
	fromString(version);
}

Version::Version()
	: major(0)
	, minor(0)
	, patch(0)
	, build(0)
{

}

bool Version::operator < (const Version& other)
{
	if (major < other.major)
		return true;
	if (minor < other.minor)
		return true;
	if (patch < other.patch)
		return true;
	if (build < other.build)
		return true;
	return false;
}

bool Version::operator > (const Version& other) 
{
	if (other.major < major)
		return true;
	if (other.minor < minor)
		return true;
	if (other.patch < patch)
		return true;
	if (other.build < build)
		return true;
	return false;
}

bool Version::operator >= (const Version& other) 
{
	return !( *this < other );
}

bool Version::operator <= (const Version& other) 
{
	return !( *this > other );
}

bool Version::operator == (const Version& other)
{
	return major == other.major
		&& minor == other.minor
		&& patch == other.patch
		&& build == other.build;
}

std::string Version::toString() 
{
	std::string version;

	if(build)
		version = fmt::format("{0}.{1}.{2}.{2}", major, minor, patch, build);
	else
		version = fmt::format("{0}.{1}.{2}", major, minor, patch);

	return version;
}

void Version::fromString(std::string version)
{
	major = minor = patch = build = 0;

	if(version[0] == 'v') {
		std::sscanf(version.c_str(), "v%d.%d.%d.%d", &major, &minor, &patch, &build);
	} else {
		std::sscanf(version.c_str(), "%d.%d.%d.%d", &major, &minor, &patch, &build);
	}
}
