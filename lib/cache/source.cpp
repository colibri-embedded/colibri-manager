/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file source.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#include <cache/source.hpp>
#include <iostream>

using namespace ce::cache;

Source::Source(const std::string& _arch, 
	const std::string& _key, 
	const std::vector<std::string>& _branches, 
	const std::vector<std::string>& _types, 
	const std::string& _url, 
	const std::string& _name
	)
	: arch(_arch), key(_key), url(_url), name(_name)
{
	for(auto &b : _branches)
		branches.push_back(b);
	for(auto &t: _types)
		types.push_back(t);
}

std::string Source::toString() const
{
	std::string str = arch + " @ " + url + " [";
	for(auto &branch: branches) {
		str += branch + " ";
	}
	str += "]";
	return str;
}