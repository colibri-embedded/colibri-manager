/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file item.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#include <cache/item.hpp>

using namespace ce::cache;

json Item::toJson()
{
	json data;
	data["name"] = name;
	data["branch"] = branch;
	data["hashId"] = hashId;
	data["latest"] = latest;
	data["priority"] = priority;
	data["optional"] = optional;
	data["fileName"] = fileName;
	data["fileHashId"] = fileHashId;
	data["fileChecksum"] = fileChecksum;
	if(alg == Algorithm::MD5)
		data["algorithm"] = "MD5";
	else if(alg == Algorithm::SHA1) 
		data["algorithm"] = "SHA1";
	data["changelog"] = changelog;

	return data;
}

bool Item::fromJson(json &obj)
{
	if(obj["name"] != nullptr)
		name = obj["name"];

	if(obj["branch"] != nullptr)
		branch = obj["branch"];

	if(obj["hashId"] != nullptr)
		hashId = obj["hashId"];

	if(obj["latest"] != nullptr)
		latest = obj["latest"];

	if(obj["priority"] != nullptr)
		priority = obj["priority"];

	if(obj["optional"] != nullptr)
		optional = obj["optional"];

	if(obj["fileName"] != nullptr)
		fileName = obj["fileName"];

	if(obj["fileHashId"] != nullptr)
		fileHashId = obj["fileHashId"];

	if(obj["fileChecksum"] != nullptr)
		fileChecksum = obj["fileChecksum"];

	if(obj["algorithm"] != nullptr) {
		if(obj["algorithm"] == "MD5") {
			alg = Algorithm::MD5;
		} else if (obj["algorithm"] == "SHA1") {
			alg = Algorithm::SHA1;
		}
	}

	if(obj["changelog"] != nullptr)
		changelog = obj["changelog"];

	return true;
}