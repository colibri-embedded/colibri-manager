/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file cache.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <algorithm>
#include <cache/cache.hpp>
#include <fstream>
#include <helper/os_helpers.hpp>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace std;
using namespace helper;
using namespace ce::cache;

Cache::Cache(std::shared_ptr<logger::Base>& _logger)
  : m_logger(_logger)
{
    m_last_sync = from_iso8601("2019-01-01T00:00:00Z");
}

std::string Cache::to_iso8601(std::chrono::system_clock::time_point& tp)
{
    auto itt = std::chrono::system_clock::to_time_t(tp);
    std::ostringstream ss;
    ss << std::put_time(localtime(&itt), "%FT%T");
    return ss.str();
}

std::chrono::system_clock::time_point Cache::from_iso8601(const std::string& iso)
{
    struct std::tm tm = {};
    strptime(iso.c_str(), "%Y-%m-%dT%T", &tm);
    return std::chrono::system_clock::from_time_t(mktime(&tm));
}

bool Cache::load(const string& filename)
{
    m_logger->debug(fmt::format("Cache::load from \"{0}\"", filename));

    json data;
    try {
        ifstream ifs(filename);
        ifs >> data;
    } catch (exception& e) {
        // handle error
        m_logger->warn(fmt::format("Cannot load cache file \"{0}\"", filename));
        return false;
    }

    bool result = false;

    json cache = data["cache"];
    if (cache != nullptr) {
        json online = cache["online"];
        if (online != nullptr) {
            for (auto& repo : online) {
                // cout << repo["name"] << endl;
                auto new_repo = new Repository();
                if (new_repo->fromJson(repo)) {
                    addRepository(new_repo);
                } else {
                    delete new_repo;
                }
            }
            result = true;
        }
        json last_sync = cache["last_sync"];
        if (last_sync != nullptr) {
            auto last_sync_s = last_sync.get<std::string>();
            m_last_sync      = from_iso8601(last_sync_s);
        }
    }

    return result;
}

bool Cache::store(const string& filename)
{
    m_logger->debug(fmt::format("Cache::store to \"{0}\"", filename));

    json data;
    data["cache"]["online"] = json::array();

    for (auto& repo : repositories) {
        data["cache"]["online"].push_back(repo->toJson());
    }

    data["cache"]["last_sync"] = to_iso8601(m_last_sync);

    try {
        auto dir = os::dirName(filename);
        os::mkDir(dir, true);
        ofstream ofs(filename);
        // Note: dump(4) activates prettyprint option
        // which causes segfaul for unknown reason (in some cases)
        ofs << data.dump() << endl;
        ofs.close();
    } catch (exception& e) {
        return false;
    }

    return true;
}

void Cache::clear()
{
    m_logger->debug(fmt::format("Cache::clear"));

    clearSources();
    clearRepositories();
}

void Cache::clearSources()
{
    m_logger->debug(fmt::format("Cache::clearSources"));
    sources.clear();
}

void Cache::clearRepositories()
{
    m_logger->debug(fmt::format("Cache::clearRepositories"));
    repositories.clear();
}

sptrSource Cache::addSource(sptrSource source)
{
    m_logger->debug(fmt::format("Cache::addSource {0}", source->toString()));
    auto existing = findSource(source->name);
    if ((bool)existing) {
        *existing = *source;
        return existing;
    } else {
        sources.push_back(source);
        return source;
    }
}

sptrSource Cache::removeSource(const std::string& name)
{
    auto existing = findSource(name);
    sources.erase(std::remove_if(sources.begin(), sources.end(), [&name](sptrSource& el) { return el->name == name; }),
                  sources.end());
    return existing;
}

sptrSource Cache::findSource(const std::string& name)
{
    for (auto source : sources) {
        if (source->name == name)
            return source;
    }
    return nullptr;
}

sptrRepository Cache::addRepository(Repository* repo)
{
    m_logger->debug(fmt::format("Cache::addRepository {0}", repo->toString()));
    auto sptr = sptrRepository(repo);
    repositories.push_back(sptr);
    return sptr;
}

const vector<sptrSource>& Cache::getSources()
{
    return sources;
}

const vector<sptrRepository>& Cache::getRepositories()
{
    return repositories;
}

void Cache::update(json& obj, const string& url, const vector<string>& branches)
{
    auto items = obj["items"];
    auto repos = obj["repos"];

    if (items != nullptr && items.size() > 0 && obj["name"] != nullptr && obj["type"] != nullptr) {

        auto repo = addRepository(new Repository{
            obj["name"],
            obj["type"],
            url,
        });

        repo->branches = branches;

        for (auto& item : items) {
            unsigned priority = std::atoi(item["priority"].get<std::string>().c_str());
            auto new_item =
                new Item{ item["name"], priority, item["hashId"], item["branch"]["name"], item["optional"] };

            auto latest = item["latest"];
            if (latest != nullptr) {
                new_item->latest    = latest["version"];
                new_item->changelog = latest["changelog"];
                auto files          = latest["files"];
                if (files.size() > 0) {
                    new_item->fileName     = files[0]["name"];
                    new_item->fileHashId   = files[0]["hashId"];
                    new_item->fileChecksum = files[0]["checksum"];
                    if (files[0]["algorithm"] == "MD5") {
                        new_item->alg = Algorithm::MD5;
                    } else if (files[0]["algorithm"] == "SHA1") {
                        new_item->alg = Algorithm::SHA1;
                    }
                }
                auto deps = item["deps"];
                if (deps != nullptr) {
                    for (auto& dep : deps) {
                        new_item->deps.push_back(Dependency{ dep["name"], dep["condition"], dep["type"] });
                    }
                }
            }

            repo->addItem(new_item);
        }
    }

    if (repos != nullptr && repos.size() > 0) {
        for (auto& repo : repos) {
            update(repo, url, branches);
        }
    }
}

shared_ptr<Item> Cache::getLatest(const string& name, const string& type, const string& arch)
{
    auto result = shared_ptr<Item>(nullptr);

    vector<shared_ptr<Item>> items;

    for (auto& repo : repositories) {
        if (repo->type == type) {
            // Collect item candidates
            for (auto& item : repo->items) {
                if (item->name == name) {
                    items.push_back(item);
                }
            }

            // map branch priority
            map<string, int> priority;
            int p = repo->branches.size();
            for (auto& branch : repo->branches) {
                priority[branch] = p--;
            }

            // select one item from collection
            int result_priority = 0;
            for (auto& item : items) {
                auto v2 = Version(item->latest);
                int pri = priority[item->branch];

                // cout << item->name << " " << item->latest  << " @ " << item->branch << "\n";

                if (result) {
                    auto v1 = Version(result->latest);
                    if (v1 == v2) {
                        // if version are equal choose item from higher priority branch
                        if (pri > result_priority) {
                            result_priority = pri;
                            result          = item;
                        }
                    } else if (v2 > v1) {
                        result          = item;
                        result_priority = pri;
                    }
                } else {
                    result_priority = pri;
                    result          = item;
                }
            }
        }
    }

    return result;
}

void Cache::setLastSyncToNow()
{
    m_last_sync = std::chrono::system_clock::now();
}

std::chrono::system_clock::time_point Cache::getLastSyncTime()
{
    return m_last_sync;
}