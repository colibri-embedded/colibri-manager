/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file manager.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
// System
#include <exception>
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
// 3rdparty
#define FMT_HEADER_ONLY
#include <INIReader.h>
#include <fmt/format.h>
// local
#include <manager.hpp>
#include <manager_impl.hpp>

using namespace ce;
using namespace ce::logger;

Manager::Manager(const std::string& config_file, std::shared_ptr<logger::Base> logger, task::Callbacks callbacks)
{
    impl = std::make_unique<ManagerImpl>(config_file, logger, callbacks);
}

Manager::~Manager() = default;

/**
 * List all installed items.
 */
bool Manager::list(const std::string& item_type, const std::string& item_arch, bool only_active)
{
    return impl->list(item_type, item_arch, only_active);
}

/**
 * Search for available items in the repository.
 */
bool Manager::search(const std::vector<std::string>& item_names,
                     const std::string& item_type,
                     const std::string& item_arch)
{
    return impl->search(item_names, item_type, item_arch);
}

/**
 * Install items from repository or local filesystem.
 */
bool Manager::install(const std::vector<std::string>& item_names,
                      const std::string& item_type,
                      const std::string& item_arch,
                      bool force,
                      bool is_local,
                      bool no_check)
{
    return impl->install(item_names, item_type, item_arch, force, is_local, no_check);
}

/**
 * Remove installed items.
 */
bool Manager::remove(const std::vector<std::string>& item_names,
                     const std::string& item_type,
                     const std::string& item_arch,
                     bool force)
{
    return impl->remove(item_names, item_type, item_arch, force);
}

/**
 * Restore items to factory version.
 */
bool Manager::restore(const std::vector<std::string>& item_names,
                      const std::string& item_type,
                      const std::string& item_arch,
                      bool force)
{
    return impl->restore(item_names, item_type, item_arch, force);
}

/**
 * Restore file from filesystem to factory version.
 */
bool Manager::restoreFile(const std::string& fileName)
{
    return impl->restoreFile(fileName);
}

/**
 * Activate items.
 */
bool Manager::activate(const std::vector<std::string>& item_names,
                       const std::string& item_type,
                       const std::string& item_arch)
{
    return impl->activate(item_names, item_type, item_arch);
}

/**
 * Deactivate items.
 */
bool Manager::deactivate(const std::vector<std::string>& item_names,
                         const std::string& item_type,
                         const std::string& item_arch)
{
    return impl->deactivate(item_names, item_type, item_arch);
}

/**
 * Update items from repository.
 */
bool Manager::update(const std::vector<std::string>& item_names,
                     const std::string& item_type,
                     const std::string& item_arch,
                     bool force,
                     bool no_check)
{
    return impl->update(item_names, item_type, item_arch, force, no_check);
}

/**
 * Synchronize local cache with online repository.
 */
bool Manager::sync()
{
    return impl->sync();
}

bool Manager::onboot()
{
    return impl->onboot();
}

bool Manager::finish()
{
    return impl->finish();
}

bool Manager::reload()
{
    return impl->reload();
}

std::vector<task::sptrTask> Manager::generateSyncTasks()
{
    return impl->generateSyncTasks();
}

std::vector<task::sptrTask> Manager::generateTasks(const std::vector<task::Request>& requests, bool no_check)
{
    return impl->generateTasks(requests, no_check);
}

bool Manager::executeTasks(const std::vector<task::sptrTask>& tasks)
{
    return impl->executeTasks(tasks);
}

std::vector<std::string> Manager::getItemTypes()
{
    return impl->getItemTypes();
}

std::vector<ce::Item> Manager::getItems(const std::string& item_type, const std::string& item_arch)
{
    return impl->getItems(item_type, item_arch);
}

std::chrono::system_clock::time_point Manager::getLastSyncTime()
{
    return impl->getLastSyncTime();
}

const std::vector<ce::cache::sptrSource>& Manager::getSources()
{
    return impl->getSources();
}

bool Manager::addSource(ce::cache::sptrSource data)
{
    return impl->addSource(data);
}

bool Manager::removeSource(const std::string& name)
{
    return impl->removeSource(name);
}