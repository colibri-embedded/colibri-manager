/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file manager.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
// System
#include <algorithm>
#include <exception>
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
// 3rdparty
#define FMT_HEADER_ONLY
#include <INIReader.h>
#include <fmt/format.h>
// local
#include <cache/cache.hpp>
#include <manager_impl.hpp>

#include <network/endpoints.h>
#include <network/graphql.h>
#include <network/http_client.hpp>

#include <helper/os_helpers.hpp>
#include <helper/string_helpers.hpp>

#include <handler/base.hpp>
#include <handler/boot.hpp>
#include <handler/bundle.hpp>
#include <handler/external.hpp>
#include <handler/file.hpp>

#include <checksum/md5.hpp>
#include <checksum/sha1.hpp>

#include <logger/none.hpp>

#include <task/activate.hpp>
#include <task/deactivate.hpp>
#include <task/download.hpp>
#include <task/install.hpp>
#include <task/remove.hpp>
#include <task/restore.hpp>
#include <task/sync.hpp>
#include <task/task.hpp>

using namespace helper;
using namespace ce;
using namespace ce::logger;
using namespace ce::task;

#define REQUIRE_ROOT                                                                                \
    if (!isRoot()) {                                                                                \
        m_logger->error(fmt::format("Root permission needed. Run this command with \"sudo ...\"")); \
        return false;                                                                               \
    }

ManagerImpl::ManagerImpl(const std::string& config_file,
                         std::shared_ptr<logger::Base> logger,
                         task::Callbacks callbacks)
{
    m_callbacks = callbacks;

    if (!(bool)(logger)) {
        m_logger = std::make_shared<None>(logger::LogLevel::DEBUG);
    } else {
        m_logger = logger;
    }

    m_cache = std::make_shared<ce::cache::Cache>(m_logger);

    m_config_file = config_file;
    loadSettings(config_file);
    m_cache->load(m_cache_file);
}

ManagerImpl::~ManagerImpl() = default;

bool ManagerImpl::isRoot()
{
    return getuid() == ROOT_ID;
}

task::Callbacks ManagerImpl::getCallbacks()
{
    return m_callbacks;
}

void ManagerImpl::setCallbacks(task::Callbacks callbacks)
{
    m_callbacks = callbacks;
}

bool ManagerImpl::loadSettings(const std::string& config_file)
{
    INIReader reader(config_file);
    if (reader.ParseError() < 0) {
        m_logger->error(fmt::format("Cannot load \"{0}\" configuration file.", config_file));
        return false;
    }

    m_cache_file = reader.Get("files", "cache_file", DEFAULT_CACHE_FILE);

    m_bundle_info_dir    = reader.Get("directories", "bundle_info_dir", DEFAULT_BUNDLE_INFO_DIR);
    m_bundles_dir        = reader.Get("directories", "bundles_dir", DEFAULT_BUNDLES_DIR);
    m_active_bundles_dir = reader.Get("directories", "active_bundles_dir", DEFAULT_ACTIVE_BUNDLES_DIR);
    m_boot_dir           = reader.Get("directories", "boot_dir", DEFAULT_BOOT_DIR);

    m_tmp_dir      = reader.Get("directories", "tmp_dir", TMP_DIR);
    m_log_dir      = reader.Get("directories", "log_dir", LOG_DIR);
    m_sources_dir  = reader.Get("directories", "sources_dir", DEFAULT_SOURCES_DIR);
    m_handlers_dir = reader.Get("directories", "handlers_dir", DEFAULT_HANDLERS_DIR);

    // Bundles handler
    auto bundles = new ce::handler::Bundle(m_logger);
    auto base    = dynamic_cast<ce::handler::Base*>(bundles);
    bundles->setBaseDirs(m_bundles_dir, m_bundle_info_dir, m_active_bundles_dir);
    m_handlers.push_back(std::unique_ptr<ce::handler::Base>(base));
    // Bootfiles handler
    auto bootfiles = new ce::handler::Boot(m_logger);
    base           = dynamic_cast<ce::handler::Base*>(bootfiles);
    bootfiles->setBaseDirs(m_boot_dir);
    m_handlers.push_back(std::unique_ptr<ce::handler::Base>(base));
    // Filesystem file handlers
    auto files = new ce::handler::File(m_logger);
    base       = dynamic_cast<ce::handler::Base*>(files);
    m_handlers.push_back(std::unique_ptr<ce::handler::Base>(base));

    loadAllRepositories();
    loadAllExternals();

    return true;
}

bool ManagerImpl::loadAllRepositories()
{
    m_logger->debug(fmt::format("ManagerImpl::loadAllRepositories"));

    try {
        auto files = os::getDirectoryFiles(m_sources_dir);
        for (auto& file : *files) {

            if (os::fileExtension(file) == ".conf") {
                std::string source_file = m_sources_dir + "/" + file;
                m_logger->debug(fmt::format("  Found source file \"{0}\"", source_file));
                if (!loadRepository(source_file)) {
                    throw std::runtime_error(fmt::format("Error loading \"{0}\"", source_file));
                }
            }
        }

        return true;
    } catch (std::exception& e) {
        m_logger->error(e.what());
    }
    return false;
}

bool ManagerImpl::loadRepository(const std::string& file_name)
{
    INIReader reader(file_name);
    if (reader.ParseError() < 0) {
        m_logger->error(fmt::format("Cannot load \"{0}\" source file.", file_name));
        return false;
    }

    auto sections = reader.Sections();
    if (sections.size() < 1) {
        m_logger->error(fmt::format("Default section is missing in \"{0}\" source file.", file_name));
        return false;
    }

    auto section = *sections.begin();

    if (!reader.HasKey(section, "key")) {
        m_logger->error(fmt::format("\"key\" value is not defined in \"{0}\" source file.", file_name));
        return false;
    }

    if (!reader.HasKey(section, "arch")) {
        m_logger->error(fmt::format("\"arch\" value is not defined in \"{0}\" source file.", file_name));
        return false;
    }

    if (!reader.HasKey(section, "url")) {
        m_logger->error(fmt::format("\"url\" value is not defined in \"{0}\" source file.", file_name));
        return false;
    }

    if (!reader.HasKey(section, "branch") && !reader.HasKey(section, "branches")) {
        m_logger->error(fmt::format("\"branch\" value is not defined in \"{0}\" source file.", file_name));
        return false;
    }

    if (!reader.HasKey(section, "types")) {
        m_logger->error(fmt::format("\"types\" value is not defined in \"{0}\" source file.", file_name));
        return false;
    }

    // Backward compatibility
    std::string branches_raw = "stable";
    if (reader.HasKey(section, "branch")) {
        branches_raw = reader.Get(section, "branch", "stable");
    } else if (reader.HasKey(section, "branches")) {
        branches_raw = reader.Get(section, "branches", "stable");
    }

    auto key       = reader.Get(section, "key", "");
    auto arch      = reader.Get(section, "arch", "");
    auto url       = reader.Get(section, "url", "");
    auto branches  = str::split(branches_raw, ',');
    auto types_raw = reader.Get(section, "types", "");
    auto types     = str::split(types_raw, ',');
    auto name      = str::split(os::baseName(file_name), '.')[0];

    auto source = std::make_shared<ce::cache::Source>(arch, key, branches, types, url, name);
    m_cache->addSource(source);

    return true;
}

bool ManagerImpl::loadAllExternals()
{
    try {
        auto files = os::getDirectoryFiles(m_handlers_dir);
        for (auto& file : *files) {
            auto script_path = os::pathJoin({ m_handlers_dir, file });
            auto base        = dynamic_cast<ce::handler::Base*>(new ce::handler::External(file, script_path, m_logger));
            m_handlers.push_back(std::unique_ptr<ce::handler::Base>(base));
        }
        return true;
    } catch (std::exception& e) {
        m_logger->error(fmt::format(e.what()));
    }
    return false;
}

std::vector<std::string> ManagerImpl::getItemTypes()
{
    std::vector<std::string> types;

    for (auto& s : m_cache->getSources()) {
        for (auto& t : s->types) {
            types.push_back(t);
        }
    }

    return types;
}

std::vector<ce::Item> ManagerImpl::getItems(const std::string& item_type, const std::string& item_arch)
{
    std::vector<ce::Item> result;

    auto handler = getHandler(item_type, item_arch);
    if (!(bool)handler) {
        throw std::runtime_error(
            fmt::format("No handler found for type \"{0}\" of arch \"{1}\"", item_type, item_arch));
    }

    auto items = handler->installed(item_arch);
    for (auto& item : *items) {
        std::string latest_version;
        std::string changelog;
        unsigned state = item.state;
        auto online    = m_cache->getLatest(item.name, item_type, item.arch);
        if ((bool)online) {
            state |= ce::handler::ONLINE;
            if (online->optional)
                state |= ce::handler::OPTIONAL;
            latest_version = online->latest;
            changelog      = online->changelog;
            // TODO: get deps
        }
        result.push_back(ce::Item{
            item.name,
            item_type,
            item.arch,
            item.priority,
            (state & ce::handler::OPTIONAL),
            item.version,
            latest_version,
            changelog
            // {} TODO: deps
        });
    }

    return result;
}

/**
 * Get handler object for given item type.
 */
ce::handler::sptrBase ManagerImpl::getHandler(const std::string& item_type, const std::string& item_arch)
{
    m_logger->debug(fmt::format("ManagerImpl::getHandler for {0}/{1}", item_type, item_arch));
    for (auto handler : m_handlers) {
        m_logger->debug(fmt::format("check: {}", handler->name()));
        if (handler->supportsType(item_type) && handler->supportsArch(item_arch))
            return handler;
    }
    m_logger->debug(fmt::format("ManagerImpl::getHandler No handler found"));
    return nullptr;
}

void ManagerImpl::clearInstallCache()
{
    for (auto handler : m_handlers) {
        handler->clear_cached();
    }
}

/**
 * List all installed items.
 */
bool ManagerImpl::list(const std::string& item_type, const std::string& item_arch, bool only_active)
{
    auto handler = getHandler(item_type, item_arch);
    if (!(bool)handler) {
        m_logger->error(fmt::format("No handler found for type \"{0}\" of arch \"{1}\"", item_type, item_arch));
        return false;
    }

    if (!handler->supportsCommand("installed")) {
        m_logger->error(fmt::format("Handler for type \"{0}\" does not support \"list\" operation", item_type));
        return false;
    }

    auto items = handler->installed(item_arch, only_active);
    for (auto& item : *items) {
        std::string latest_version;
        std::string changelog;
        unsigned state = item.state;
        auto online    = m_cache->getLatest(item.name, item_type, item.arch);
        if ((bool)online) {
            m_logger->debug(fmt::format("Online version of \"{0}\" available", item.name));
            state |= ce::handler::ONLINE;
            if (online->optional)
                state |= ce::handler::OPTIONAL;
            latest_version = online->latest;
            changelog      = online->changelog;
        } else {
            m_logger->debug(fmt::format("No online version of \"{0}\"", item.name));
        }
        m_logger->item(item.name, item_type, item.version, latest_version, changelog, state);
    }

    return true;
}

/**
 * Search for available items in the repository.
 */
bool ManagerImpl::search(const std::vector<std::string>& item_names,
                         const std::string& item_type,
                         const std::string& item_arch)
{
    m_logger->debug(fmt::format("ManagerImpl::search"));

    bool show_all = item_type == "all";
    for (auto& repo : m_cache->getRepositories()) {
        if (repo->type != item_type && !show_all)
            continue;

        auto handler = getHandler(repo->type, item_arch);

        for (auto& item : repo->items) {
            std::string local_version;
            unsigned state = ce::handler::ONLINE;
            if (item->optional)
                state |= ce::handler::OPTIONAL;

            if ((bool)handler) {
                auto items = handler->installed(item_arch);
                for (auto& local_item : *items) {
                    if (local_item.name == item->name) {
                        local_version = local_item.version;
                        state |= local_item.state;
                    }
                }
            }

            m_logger->search_result(item->name, repo->type, item->branch, item->latest, local_version, state);
        }
    }
}

std::vector<sptrTask> ManagerImpl::generateTasks(const std::vector<Request>& requests, bool no_check)
{
    m_logger->debug("ManagerImpl::generateTasks start");

    std::vector<sptrTask> tasks;
    std::vector<std::string> used_handlers;

    for (const auto& req : requests) {
        auto handler = getHandler(req.item.type, req.item.arch);
        if ((bool)handler) {

            if (std::find(used_handlers.begin(), used_handlers.end(), handler->name()) == used_handlers.end()) {
                used_handlers.push_back(handler->name());
            }

            bool op_supported = false;
            switch (req.operation) {
                case Type::INSTALL:
                case Type::UPDATE:
                    op_supported = handler->supportsCommand("install");
                    break;
                case Type::REMOVE:
                    op_supported = handler->supportsCommand("remove");
                    break;
                case Type::RESTORE:
                    op_supported = handler->supportsCommand("restore");
                    break;
                case Type::ACTIVATE:
                    op_supported = handler->supportsCommand("activate");
                    break;
                case Type::DEACTIVATE:
                    op_supported = handler->supportsCommand("deactivate");
                    break;
                default:;
                    op_supported = false;
            }

            if (!op_supported) {
                throw std::runtime_error(
                    fmt::format("Handler for type \"{0}\" does not support \"install\" operation", req.item.type));
            }

            if (req.operation == Type::INSTALL || req.operation == Type::UPDATE) {
                ce::cache::Version local;
                auto installed = handler->installed(req.item.arch);

                if (req.is_local) {
                    // Local files have no access to dependency meta data so they will be installed without
                    // any dependency check
                    bool is_installed = false;
                    unsigned priority = 0;
                    std::string name;
                    std::string version;
                    auto filename = os::baseName(req.item.name);

                    m_logger->debug(fmt::format("Is Local file: {0}", filename));

                    if (handler->splitFilename(filename, priority, name, version)) {
                        ce::cache::Version item_version(version);

                        for (auto& item : *installed) {
                            if (item.name == name) {
                                local.fromString(item.version);
                                is_installed = true;
                            }
                        }

                        if (is_installed) {
                            m_logger->debug("Item already installed");
                            // check version
                            if ((item_version > local) or no_check) {
                                auto di = std::make_shared<Install>(
                                    handler, name, req.item.type, req.item.arch, version, req.is_forced);
                                di->setFileName(req.item.name);
                                tasks.push_back(di);
                            } else {
                                // skip
                                m_logger->warn(fmt::format(
                                    "Provided version of \"{0}\" is older then installed, skipping", filename));
                            }
                        } else {
                            m_logger->debug("Item not installed, will do it now");
                            // just install it
                            m_logger->debug(fmt::format("Make INSTALL task: {}/{} {} {} {}",
                                                        req.item.name,
                                                        name,
                                                        req.item.type,
                                                        req.item.arch,
                                                        version));

                            auto di = std::make_shared<Install>(
                                handler, name, req.item.type, req.item.arch, version, req.is_forced);

                            di->setFileName(req.item.name);
                            tasks.push_back(di);
                        }
                    } else {
                        throw std::runtime_error(fmt::format("Cannot parse filename: {}", filename));
                    }
                } else {
                    bool is_installed = false;

                    for (auto& item : *installed) {
                        if (item.name == req.item.name) {
                            local.fromString(item.version);
                            is_installed = true;
                        }
                    }

                    auto online = m_cache->getLatest(req.item.name, req.item.type, req.item.arch);
                    if (online) {
                        auto latest = ce::cache::Version(online->latest);

                        if (online->fileHashId == "" or online->fileName == "") {
                            throw std::runtime_error(
                                fmt::format("online: missing fileHashId and fileName for \"{0}\" item", req.item.name));
                        }

                        if ((latest > local) or no_check) {
                            auto di = std::make_shared<Install>(handler,
                                                                online->name,
                                                                req.item.type,
                                                                req.item.arch,
                                                                online->latest,
                                                                online->priority,
                                                                req.is_forced);
                            auto dt =
                                std::make_shared<Download>(di,
                                                           online->fileName,
                                                           online->repo->url + DOWNLOAD_FILE + "/" + online->fileHashId,
                                                           online->fileChecksum,
                                                           static_cast<unsigned>(online->alg));

                            tasks.push_back(dt);
                            tasks.push_back(di);
                        } else {
                            // skipping
                            m_logger->warn(fmt::format("Package \"{0}\" already up-to-date", req.item.name));
                        }
                    } else {
                        // skipping
                        throw std::runtime_error(
                            fmt::format("No package \"{0}\" found in the repository", req.item.name));
                    }
                }
            } else if (req.operation == Type::REMOVE) {
                auto t = std::make_shared<Remove>(handler, req);
                tasks.push_back(t);
            } else if (req.operation == Type::RESTORE) {
                auto t = std::make_shared<Restore>(handler, req);
                tasks.push_back(t);
            } else if (req.operation == Type::ACTIVATE) {
                auto t = std::make_shared<Activate>(handler, req);
                tasks.push_back(t);
            } else if (req.operation == Type::DEACTIVATE) {
                auto t = std::make_shared<Deactivate>(handler, req);
                tasks.push_back(t);
            }
        } else {
            throw std::runtime_error(
                fmt::format("No handler for item \"{}\" of type \"{}\"", req.item.name, req.item.type));
        }
    }

    for (const auto& handler_name : used_handlers) {
        for (auto h : m_handlers) {
            if (handler_name == h->name()) {
                h->plan_install(tasks, m_cache);
            }
        }
    }

    // Sort tasks by type and priority
    std::sort(tasks.begin(), tasks.end(), [](const sptrTask& a, const sptrTask& b) -> bool {
        if (a->getType() != b->getType()) {
            return a->getType() < b->getType();
        } else {
            if (a->getType() == Type::INSTALL || a->getType() == Type::DOWNLOAD) {
                return a->getItem().priority < b->getItem().priority;
            }
            return false;
        }
    });

    // Assign each task an unique ID number
    // and register callbacks
    unsigned id = 0;
    for (auto& task : tasks) {
        task->setId(id++);
        task->setCallbacks(m_callbacks);
    }

    m_logger->debug("ManagerImpl::generateTasks finish");

    return tasks;
}

bool ManagerImpl::executeTasks(const std::vector<sptrTask>& tasks)
{
    m_logger->debug("ManagerImpl::executeTasks");

    clearInstallCache();

    if (m_callbacks.tasks_created) {
        // Trigger tasks.created signal
        m_callbacks.tasks_created(tasks);
    }

    for (auto h : m_handlers) {
        if (!h->pre_install_steps(tasks)) {
            throw std::runtime_error(fmt::format("Pre-install steps failed"));
        }
    }

    bool aborting = false;
    for (const auto& task : tasks) {
        if (aborting) {
            task->setStatus(task::Status::ABORTED);
            continue;
        }

        task->setStatus(task::Status::WORKING);
        auto result = task->execute(m_logger, m_tmp_dir);
        if (result) {
            task->setStatus(task::Status::DONE);
        } else {
            task->setStatus(task::Status::FAILED);
            aborting = true;
        }
    }

    if (m_callbacks.tasks_finished) {
        // Trigger tasks.finished signal
        m_callbacks.tasks_finished(!aborting);
    }

    clearInstallCache();

    return !aborting;
}

/**
 * Install items from repository or local filesystem.
 */
bool ManagerImpl::install(const std::vector<std::string>& item_names,
                          const std::string& item_type,
                          const std::string& item_arch,
                          bool is_forced,
                          bool is_local,
                          bool no_check)
{
    REQUIRE_ROOT;

    auto handler = getHandler(item_type, item_arch);
    if (!(bool)handler) {
        m_logger->error(fmt::format("No handler found for type \"{0}\" of arch \"{1}\"", item_type, item_arch));
        return false;
    }

    if (!handler->supportsCommand("install")) {
        m_logger->error(fmt::format(
            "Handler for type \"{0}\" and \"{1}\" arch does not support \"install\" operation", item_type, item_arch));
        return false;
    }

    try {
        std::vector<Request> requests;
        for (auto& item_name : item_names) {
            requests.push_back({ Type::INSTALL, { item_name, item_type, item_arch }, is_forced, is_local });
        }
        auto tasks = generateTasks(requests, no_check);
        return executeTasks(tasks); // return true on success
    } catch (std::exception& e) {
        m_logger->error(e.what());
    }

    return false;
}

/**
 * Update items from repository.
 */
bool ManagerImpl::update(const std::vector<std::string>& item_names,
                         const std::string& item_type,
                         const std::string& item_arch,
                         bool is_forced,
                         bool no_check)
{
    return install(item_names, item_type, item_arch, is_forced, false, no_check);
}

/**
 * Remove installed items.
 */
bool ManagerImpl::remove(const std::vector<std::string>& item_names,
                         const std::string& item_type,
                         const std::string& item_arch,
                         bool is_forced)
{
    REQUIRE_ROOT;

    auto handler = getHandler(item_type, item_arch);
    if (!(bool)handler) {
        m_logger->error(fmt::format("No handler found for type \"{0}\" of arch \"{1}\"", item_type, item_arch));
        return false;
    }

    if (!handler->supportsCommand("remove")) {
        m_logger->error(fmt::format("Handler for type \"{0}\" does not support \"remove\" operation", item_type));
        return false;
    }

    try {
        std::vector<Request> requests;
        for (auto& item_name : item_names) {
            requests.push_back({ Type::REMOVE, { item_name, item_type, item_arch }, is_forced, false });
        }
        auto tasks = generateTasks(requests);
        return executeTasks(tasks); // return true on success
    } catch (std::exception& e) {
        m_logger->error(e.what());
    }

    return false;
}

/**
 * Restore items to factory version.
 */
bool ManagerImpl::restore(const std::vector<std::string>& item_names,
                          const std::string& item_type,
                          const std::string& item_arch,
                          bool is_forced)
{
    REQUIRE_ROOT;

    auto handler = getHandler(item_type, item_arch);
    if (!(bool)handler) {
        m_logger->error(fmt::format("No handler found for type \"{0}\" of arch \"{1}\"", item_type, item_arch));
        return false;
    }

    if (!handler->supportsCommand("restore")) {
        m_logger->error(fmt::format("Handler for type \"{0}\" does not support \"restore\" operation", item_type));
        return false;
    }

    try {
        std::vector<Request> requests;
        for (auto& item_name : item_names) {
            requests.push_back({ Type::RESTORE, { item_name, item_type, item_arch }, is_forced, false });
        }
        auto tasks = generateTasks(requests);
        return executeTasks(tasks); // return true on success
    } catch (std::exception& e) {
        m_logger->error(e.what());
    }

    return false;
}

/**
 * Restore file from filesystem to factory version.
 */
bool ManagerImpl::restoreFile(const std::string& fileName)
{
    REQUIRE_ROOT;

    auto changesFileName = os::pathJoin({ DEFAULT_CHANGES_DIR, fileName });
    auto fileNameOnly    = os::baseName(fileName);
    auto filePathOnly    = os::dirName(fileName);
    auto whFile          = os::pathJoin({ DEFAULT_CHANGES_DIR, filePathOnly, fmt::format(".wh.{0}", fileNameOnly) });

    if (os::isFile(changesFileName)) {
        os::remove(changesFileName);
    } else if (os::isDirectory(changesFileName)) {
        os::removeDirectory(changesFileName);
    }
    os::remove(whFile);

    return true;
}

/**
 * Activate items.
 */
bool ManagerImpl::activate(const std::vector<std::string>& item_names,
                           const std::string& item_type,
                           const std::string& item_arch)
{
    REQUIRE_ROOT;

    auto handler = getHandler(item_type, item_arch);
    if (!(bool)handler) {
        m_logger->error(fmt::format("No handler found for type \"{0}\" of arch \"{1}\"", item_type, item_arch));
        return false;
    }

    if (!handler->supportsCommand("activate")) {
        m_logger->error(fmt::format("Handler for type \"{0}\" does not support \"activate\" operation", item_type));
        return false;
    }

    try {
        std::vector<Request> requests;
        for (auto& item_name : item_names) {
            requests.push_back({ Type::ACTIVATE, { item_name, item_type, item_arch }, false, false });
        }
        auto tasks = generateTasks(requests);
        return executeTasks(tasks); // return true on success
    } catch (std::exception& e) {
        m_logger->error(e.what());
    }

    return false;
}

/**
 * Deactivate items.
 */
bool ManagerImpl::deactivate(const std::vector<std::string>& item_names,
                             const std::string& item_type,
                             const std::string& item_arch)
{
    REQUIRE_ROOT;

    auto handler = getHandler(item_type, item_arch);
    if (!(bool)handler) {
        m_logger->error(fmt::format("No handler found for type \"{0}\" of arch \"{1}\"", item_type, item_arch));
        return false;
    }

    if (!handler->supportsCommand("activate")) {
        m_logger->error(fmt::format("Handler for type \"{0}\" does not support \"activate\" operation", item_type));
        return false;
    }

    try {
        std::vector<Request> requests;
        for (auto& item_name : item_names) {
            requests.push_back({ Type::DEACTIVATE, { item_name, item_type, item_arch }, false, false });
        }
        auto tasks = generateTasks(requests);
        return executeTasks(tasks); // return true on success
    } catch (std::exception& e) {
        m_logger->error(e.what());
    }

    return false;
}

std::vector<task::sptrTask> ManagerImpl::generateSyncTasks()
{
    std::vector<sptrTask> tasks;

    auto task = std::make_shared<Sync>(m_cache, m_cache_file);
    task->setId(0);
    task->setCallbacks(m_callbacks);
    tasks.push_back(task);

    return tasks;
}

/**
 * Synchronize local cache with online repository.
 */
bool ManagerImpl::sync()
{
    REQUIRE_ROOT;

    try {
        auto tasks = generateSyncTasks();
        return executeTasks(tasks);
    } catch (std::exception& e) {
        m_logger->error(e.what());
    }

    return false;
}

bool ManagerImpl::onboot()
{
    REQUIRE_ROOT;
    for (auto& handler : m_handlers) {
        handler->onboot();
    }
    return true;
}

bool ManagerImpl::finish()
{
    std::cout << m_logger->toString();
}

bool ManagerImpl::reload()
{
    clearInstallCache();
    m_cache->clearRepositories();
    m_cache->clearSources();
    m_handlers.clear();
    return loadSettings(m_config_file);
}

std::chrono::system_clock::time_point ManagerImpl::getLastSyncTime()
{
    return m_cache->getLastSyncTime();
}

const std::vector<ce::cache::sptrSource>& ManagerImpl::getSources()
{
    return m_cache->getSources();
}

bool ManagerImpl::addSource(ce::cache::sptrSource data)
{
    auto exists = m_cache->findSource(data->name);

    std::string content = "[repository]\n";
    content += fmt::format("arch={0}\n", data->arch);
    content += fmt::format("key={0}\n", data->key);
    content += fmt::format("url={0}\n", data->url);
    content += fmt::format("types={0}\n", helper::str::join(data->types, ","));
    content += fmt::format("branches={0}\n", helper::str::join(data->branches, ","));

    m_cache->addSource(data);

    auto source_file = helper::os::pathJoin({ m_sources_dir, data->name }) + ".conf";
    return helper::os::createFile(source_file, content);
}

bool ManagerImpl::removeSource(const std::string& name)
{
    auto source = m_cache->findSource(name);
    if ((bool)source) {
        m_cache->removeSource(name);
        std::cout << "removed from cache\n";
        auto source_file = helper::os::pathJoin({ m_sources_dir, name }) + ".conf";
        helper::os::remove(source_file);
        std::cout << "removed source_file " << source_file << std::endl;
        return true;
    }
    return false;
}