/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file bundle.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <checksum/md5.hpp>
#include <colibrimngr/config.h>
#include <cstdio>
#include <handler/bundle.hpp>
#include <helper/os_helpers.hpp>
#include <iostream>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace ce::handler;
using namespace helper;

Bundle::Bundle(std::shared_ptr<logger::Base> logger)
  : Base("bundle", logger)
  , bundles_dir(DEFAULT_BUNDLES_DIR)
  , bundle_info_dir(DEFAULT_BUNDLE_INFO_DIR)
  , active_bundles_dir(DEFAULT_ACTIVE_BUNDLES_DIR)
  , fn_regex("([0-9]+)-([a-zA-Z0-9-]+)-v([0-9.]+)\\.cb")
{
    valid = true;

    addSupportedCommand("onboot");
    addSupportedCommand("installed");
    addSupportedCommand("install");
    addSupportedCommand("remove");
    addSupportedCommand("restore");
    addSupportedCommand("activate");
    addSupportedCommand("deactivate");

    addSupportedType("os.bundles");
    addSupportedArch("armhf");
}

void Bundle::setBaseDirs(const std::string& basedir, const std::string& baseinfodir, const std::string& activedir)
{
    bundles_dir        = basedir;
    bundle_info_dir    = baseinfodir;
    active_bundles_dir = activedir;
}

bool Bundle::install(const std::string& item_name,
                     const std::string& item_arch,
                     const std::string& item_version,
                     const std::string& item_filename,
                     bool force)
{
    bool is_active = isActive(item_name);
    std::string current_bundle_file;
    bool is_installed = isInstalled(item_name, &current_bundle_file);
    bool postpone     = false;

    if (is_active and force) {
        // deactivate
        if (not deactivate(item_name, item_arch)) {
            output->error(fmt::format("Failed deactivating \"{0}\" bundle.", item_name));
            return false;
        }
        is_active = false;
    } else if (is_active and !force) {
        // postpone
        postpone = true;
    }

    // TODO: pre-install
    if (not runBundleScript(item_filename, item_name, "pre_install", is_active)) {
        output->error(fmt::format("Failed to run pre-install script."));
        return false;
    }

    if (is_installed) {

        if (not removeBundle(current_bundle_file, item_name, postpone)) {
            output->error(fmt::format("Failed to remove bundle \"{0}\"", item_name));
            return false;
        }
    }

    if (postpone) {
        auto fileName             = os::baseName(item_filename);
        auto removeTriggerFile    = item_filename + ".remove";
        auto dstFilename          = os::pathJoin({ bundles_dir, fileName });
        auto baseFilename         = os::pathJoin({ bundles_dir, item_name });
        auto overwriteFilename    = dstFilename + ".overwrite";
        auto md5FileName          = dstFilename + ".md5sum";
        auto overwriteMd5FileName = md5FileName + ".overwrite";
        auto checksum             = MD5::from_file(item_filename);
        auto md5FileContent       = fmt::format("{0}  {1}\n", checksum, fileName);

        remountRW();
        if (os::exists(dstFilename)) {
            // Postpone the overwrite until reboot
            os::copyFile(item_filename, overwriteFilename);
            os::changeMode(overwriteFilename, 0644);
            // Store bundle checksum to overwrite file
            os::createFile(overwriteMd5FileName, md5FileContent);
            os::changeMode(overwriteMd5FileName, 0644);
        } else {
            // Copy new bundle file to bundles partition
            os::copyFile(item_filename, dstFilename);
            os::changeMode(dstFilename, 0644);
            // Store bundle checksum
            os::createFile(md5FileName, md5FileContent);
            os::changeMode(md5FileName, 0644);
        }
        createTrigger(baseFilename, "post_install");
        remountRO();

    } else {

        auto fileName       = os::baseName(item_filename);
        auto dstFilename    = os::pathJoin({ bundles_dir, fileName });
        auto md5FileName    = dstFilename + ".md5sum";
        auto checksum       = MD5::from_file(item_filename);
        auto md5FileContent = fmt::format("{0}  {1}\n", checksum, fileName);

        remountRW();
        if (!os::copyFile(item_filename, dstFilename)) {
            remountRO();
            output->error(fmt::format("Failed to copy bundle \"{0}\" file to /bundles partition", item_name));
            return false;
        }
        os::changeMode(dstFilename, 0644);
        // Store bundle checksum
        os::createFile(md5FileName, md5FileContent);
        os::changeMode(md5FileName, 0644);
        remountRO();

        if (!postpone) {
            if (!runBundleScript(item_filename, item_name, "post_install", is_active)) {
                output->error(fmt::format("Failed to run post-install script."));
                return false;
            }

            output->info(fmt::format("{0}: Activating", item_name));
            if (!activate(item_name, item_arch)) {
                output->error(fmt::format("Failed to activate \"{0}\"", item_name));
                return false;
            }
        }
    }

    return true;
}

bool Bundle::removeBundle(const std::string& fileName, const std::string& bundle_name, bool postpone)
{
    if (postpone) {
        if (not runBundleScript(fileName, bundle_name, "postpone", true)) {
            return false;
        }
        auto filePath = os::dirName(fileName);
        auto baseName = os::pathJoin({ filePath, bundle_name });
        remountRW();
        // create remove bundle trigger
        createTrigger(fileName, "remove");
        createTrigger(baseName, "post_remove");
        auto metaCopyDir = baseName + ".meta";
        if (not storeBundleScripts(fileName, bundle_name, metaCopyDir)) {
            output->error(fmt::format("Failed to copy bundle meta data for \"{0}\"", bundle_name));
        }
        remountRO();
    } else {
        if (not storeBundleScripts(fileName, bundle_name, TMP_SCRIPTS_DIR)) {
            output->error(fmt::format("Failed storing bundle script files."));
            return false;
        }
        // run pre-remove script
        if (not runStoredBundleScript(TMP_SCRIPTS_DIR, "pre_remove")) {
            output->error(fmt::format("Failed to run pre-remove script for \"{0}\"", bundle_name));
            return false;
        }
        remountRW();
        // remove bundle file
        os::remove(fileName);
        // remove bundle meta files
        auto metaFiles = fileName + ".*";
        os::remove(metaFiles);
        remountRO();

        // run post-remove script
        if (not runStoredBundleScript(TMP_SCRIPTS_DIR, "post_remove")) {
            output->error(fmt::format("Failed to run post-remove script for \"{0}\"", bundle_name));
            return false;
        }
        // remove temporary directory
        os::removeDirectory(TMP_SCRIPTS_DIR);
    }

    return true;
}

bool Bundle::remove(const std::string& item_name, const std::string& item_arch, bool force)
{
    bool is_active    = isActive(item_name);
    bool is_installed = isInstalled(item_name);
    bool postpone     = false;

    if (not is_installed) {
        output->error(fmt::format("Bundle \"{0}\" not installed.", item_name));
        return false;
    }

    if (is_active and force) {
        deactivate(item_name, item_arch);
    } else if (is_active and !force) {
        postpone = true;
    }

    auto item_file = getBundleFileName(item_name);
    return removeBundle(item_file, item_name, postpone);
}

bool Bundle::restore(const std::string& item_name, const std::string& item_arch, bool force)
{
    auto factoryFile = getFactoryFileName(item_name);

    if (not factoryFile.empty()) {
        std::string name;
        std::string version;
        unsigned priority;

        if (splitFilename(factoryFile, priority, name, version)) {
            if (not install(name, item_arch, version, factoryFile, force)) {
                output->error(fmt::format("Failed to restore \"{0}\" to factory version.", item_name));
                return false;
            }
        } else {
            output->error(fmt::format("Factory file for \"{0}\" has invalid filename format.", item_name));
            return false;
        }
    } else {
        output->error(fmt::format("Bundle \"{0}\" is not in factory defaults.", item_name));
        return false;
    }

    return true;
}

std::shared_ptr<std::vector<Installed>> Bundle::installed(const std::string& item_arch, bool only_active)
{
    if (!(bool)_installed) {
        _installed = std::make_shared<std::vector<Installed>>();
    } else {
        return _installed;
    }
    auto bundles = os::getDirectoryFiles(bundles_dir);

    for (auto& bundle : *bundles) {
        std::string name;
        std::string version;
        unsigned priority;
        unsigned state = handler::INSTALLED;

        if (splitFilename(bundle, priority, name, version)) {
            bool active = isActive(name);
            if (active)
                state |= handler::ACTIVE;

            if (only_active) {
                if (active)
                    _installed->push_back(Installed(name, "armhf", version, priority, state));
            } else {
                _installed->push_back(Installed(name, "armhf", version, priority, state));
            }
        }
    }

    return _installed;
}

bool Bundle::splitFilename(std::string& fileName, unsigned& priority, std::string& name, std::string& version) const
{
    std::smatch match;
    if (std::regex_match(fileName, match, fn_regex)) {
        if (match.size() == 4) {
            std::string tmp = match[1];
            priority        = std::atoi(tmp.c_str());
            name            = match[2];
            version         = match[3];
            return true;
        }
    }

    return false;
}

bool Bundle::activate(const std::string& item_name, const std::string& item_arch)
{
    auto bundle = getBundleFileName(item_name);

    if (isActive(item_name)) {
        output->error(fmt::format("Bundle \"{0}\" already activated.", item_name));
        return false;
    }

    if (not bundle.empty()) {
        auto fileName   = os::baseName(bundle);
        auto activePath = os::pathJoin({ active_bundles_dir, fileName });

        if (not runBundleScript(bundle, item_name, "pre_activate")) {
            output->error(fmt::format("Failed to execute pre_activate script"));
            return false;
        }

        if (not mountBundleAt(bundle, activePath)) {
            output->error(fmt::format("Failed to mount bundle \"{0}\" at {1}", bundle, activePath));
            return false;
        }

        auto cmd = fmt::format("mount -o remount,add:1:\"{0}\" aufs /", activePath);
        if (not os::executeSimple(cmd)) {
            output->error(fmt::format("Failed to add bundle \"{0}\" to aufs", item_name));
            return false;
        }

        if (not runBundleScript(bundle, item_name, "post_activate", true)) {
            output->error(fmt::format("Failed to execute post_activate script"));
            return false;
        }
    } else {
        output->error(fmt::format("Bundle \"{0}\" not installed.", item_name));
        return false;
    }

    return true;
}

bool Bundle::deactivate(const std::string& item_name, const std::string& item_arch)
{
    auto bundle = getBundleFileName(item_name);

    if (not isActive(item_name)) {
        output->error(fmt::format("Bundle \"{0}\" already deactivated.", item_name));
        return false;
    }

    if (not bundle.empty()) {
        if (not runBundleScript(bundle, item_name, "pre_deactivate", true)) {
            output->error(fmt::format("Failed to execute pre_deactivate script"));
            return false;
        }

        // remove Bundle from aufs overlay
        auto fileName   = os::baseName(bundle);
        auto activePath = os::pathJoin({ active_bundles_dir, fileName });
        auto cmd        = fmt::format("mount -t aufs -o remount,verbose,del:\"{0}\" aufs /", activePath);

        if (not os::executeSimple(cmd)) {
            output->error(fmt::format("Failed to remove bundle from aufs."));
            return false;
        }
        // umount Bundle squashfs mount
        cmd = fmt::format("umount {0}", activePath);
        if (not os::executeSimple(cmd)) {
            output->error(fmt::format("Failed to un-mount bundle squashfs archive."));
            return false;
        }
        // remove Bundle mount path
        if (not os::removeDirectory(activePath)) {
            output->error(fmt::format("Failed to remove bundle mount-point."));
            return false;
        }

        if (not runBundleScript(bundle, item_name, "post_deactivate")) {
            output->error(fmt::format("Failed to execute post_deactivate script"));
            return false;
        }
    } else {
        output->error(fmt::format("Bundle \"{0}\" not installed.", item_name));
    }

    return true;
}

bool Bundle::storeBundleScripts(const std::string& fileName,
                                const std::string& bundle_name,
                                const std::string& storePath,
                                bool active)
{
    bool mounted = false;
    if (not active) {
        if (not mountBundleAt(fileName, TMP_MOUNT_DIR)) {
            output->error(fmt::format("Failed to mount bundle \"{0}\"", fileName));
            return false;
        }
        mounted = true;
    }
    std::string script_path;
    if (mounted)
        script_path = os::pathJoin({ TMP_MOUNT_DIR, "/var/lib/colibri/bundle", bundle_name });
    else
        script_path = os::pathJoin({ "/var/lib/colibri/bundle", bundle_name });

    os::mkDir(storePath, true);
    os::copyFiles(script_path, storePath);

    if (mounted) {
        auto cmd = fmt::format("umount {0}", TMP_MOUNT_DIR);
        if (not os::executeSimple(cmd)) {
            output->error(fmt::format("Failed to un-mount temporary bundle mount point"));
            return false;
        }
    }

    return true;
}

bool Bundle::runStoredBundleScript(const std::string& scriptPath, const std::string& scriptName, bool verbose)
{
    bool result      = true;
    auto script_path = os::pathJoin({ scriptPath, scriptName });

    if (os::isExecutable(script_path)) {
        unsigned exitCode = -1;
        auto output       = os::execute(script_path, &exitCode);
        if (verbose) {
            std::cout << output;
        }
        result = (exitCode == 0);
    }

    return result;
}

bool Bundle::runBundleScript(const std::string& fileName,
                             const std::string& bundle_name,
                             const std::string& script_name,
                             bool active,
                             bool verbose)
{
    bool mounted = false;
    if (not active) {
        if (not mountBundleAt(fileName, TMP_MOUNT_DIR)) {
            output->error(fmt::format("Failed to mount bundle \"{0}\"", fileName));
            return false;
        }
        mounted = true;
    }

    std::string script_path;
    if (mounted)
        script_path = os::pathJoin({ TMP_MOUNT_DIR, "/var/lib/colibri/bundle", bundle_name, script_name });
    else
        script_path = os::pathJoin({ "/var/lib/colibri/bundle", bundle_name, script_name });

    bool result = true;
    if (os::isExecutable(script_path)) {
        unsigned exitCode = -1;
        auto output       = os::execute(script_path, &exitCode);
        if (verbose) {
            std::cout << output;
        }
        result = (exitCode == 0);
    }

    if (mounted) {
        auto cmd = fmt::format("umount {0}", TMP_MOUNT_DIR);
        if (not os::executeSimple(cmd)) {
            output->error(fmt::format("Failed to un-mount temporary bundle mount point"));
            return false;
        }
    }

    return result;
}

bool Bundle::mountBundleAt(const std::string& fileName, const std::string& mountPoint)
{
    if (os::isFile(fileName)) {
        if (os::mkDir(mountPoint, true)) {
            auto cmd = fmt::format("mount -o loop -t squashfs \"{0}\" \"{1}\"", fileName, mountPoint);
            if (not os::executeSimple(cmd)) {
                output->error(fmt::format("Failed mounting {0}", fileName));
                return false;
            }
        } else {
            output->error(fmt::format("Failed to create mountPoint \"{0}\"", mountPoint));
            return false;
        }
    } else {
        output->error(fmt::format("\"{0}\" is not a file", fileName));
        return false;
    }

    return true;
}

void Bundle::createTrigger(const std::string& fileName, const std::string& trigger_name)
{
    auto controlFile = fileName + "." + trigger_name;
    os::createFile(controlFile, trigger_name);
}

std::string Bundle::getBundleFileName(const std::string& bundle_name)
{
    std::string bundle_file;
    if (isInstalled(bundle_name, &bundle_file)) {
        return bundle_file;
    }

    return "";
}

std::string Bundle::getFactoryFileName(const std::string& bundle_name)
{
    auto factoryDir = os::pathJoin({ DEFAULT_BOOT_DIR, "bundles" });
    auto bundles    = os::getDirectoryFiles(factoryDir);

    for (auto& bundle : *bundles) {
        std::string name;
        std::string version;
        unsigned priority;

        if (splitFilename(bundle, priority, name, version)) {
            if (name == bundle_name) {
                return os::pathJoin({ factoryDir, bundle });
            }
        }
    }

    return "";
}

bool Bundle::isActive(const std::string& bundle_name)
{
    auto files = os::getDirectoryFiles(active_bundles_dir);
    for (auto& fileName : *files) {
        auto path = os::pathJoin({ active_bundles_dir, fileName });
        if (os::isDirectory(path)) {
            std::string name;
            std::string version;
            unsigned priority;

            if (splitFilename(fileName, priority, name, version)) {
                if (name == bundle_name) {
                    return true;
                }
            }
        }
    }

    return false;
}

bool Bundle::isInstalled(const std::string& bundle_name, std::string* bundle_file)
{
    auto files = os::getDirectoryFiles(bundles_dir);
    for (auto& fileName : *files) {
        auto path = os::pathJoin({ bundles_dir, fileName });
        if (os::isFile(path)) {
            std::string name;
            std::string version;
            unsigned priority;

            if (splitFilename(fileName, priority, name, version)) {
                if (name == bundle_name) {
                    if (bundle_file != nullptr) {
                        *bundle_file = path;
                    }
                    return true;
                }
            }
        }
    }

    return false;
}

bool Bundle::remountRW()
{
    unsigned exitCode = -1;
    std::string cmd   = "mount -o remount,rw " + bundles_dir;
    os::execute(cmd, &exitCode);

    return exitCode == 0;
}

bool Bundle::remountRO()
{
    unsigned exitCode = -1;
    std::string cmd   = "mount -o remount,ro " + bundles_dir;
    os::execute(cmd, &exitCode);

    return exitCode == 0;
}

void Bundle::onboot()
{
    auto files = os::getDirectoryFiles(bundles_dir);
    remountRW();
    for (auto& file : *files) {
        std::string name;
        std::string version;
        unsigned priority;
        if (splitFilename(file, priority, name, version)) {
            auto baseName = os::pathJoin({ bundles_dir, name });
            auto metaDir  = baseName + ".meta";

            auto postRemoveTriggerFile = baseName + ".post_remove";
            if (os::exists(postRemoveTriggerFile)) {
                output->info(fmt::format("Found post_remove trigger for \"{0}\"", name));
                os::remove(postRemoveTriggerFile);
                runStoredBundleScript(metaDir, "post_remove", true);
            }

            auto postInstallTriggerFile = baseName + ".post_install";
            if (os::exists(postInstallTriggerFile)) {
                output->info(fmt::format("Found post_install trigger for \"{0}\"", name));
                os::remove(postInstallTriggerFile);
                runBundleScript(baseName, name, "post_install", true, true);
            }

            os::removeDirectory(metaDir);
        }
    }
    remountRO();
}
