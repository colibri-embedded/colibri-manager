/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file file.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <colibrimngr/config.h>
#include <handler/file.hpp>
#include <helper/os_helpers.hpp>
#include <iostream>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace ce::handler;
using namespace helper;

File::File(std::shared_ptr<logger::Base> _output)
  : Base("file", _output)
{
    valid = true;
    addSupportedCommand("restore");
    addSupportedType("file");
}

bool File::restore(std::string& fileName, const std::string& item_arch, bool force)
{
    auto changesFileName = os::pathJoin({ DEFAULT_CHANGES_DIR, fileName });
    auto fileNameOnly    = os::baseName(fileName);
    auto filePathOnly    = os::dirName(fileName);
    auto whFile          = os::pathJoin({ DEFAULT_CHANGES_DIR, filePathOnly, fmt::format(".wh.{0}", fileNameOnly) });

    // std::cout << "whFile: " << whFile << std::endl;

    if (os::isFile(changesFileName)) {
        os::remove(changesFileName);
    } else if (os::isDirectory(changesFileName)) {
        os::removeDirectory(changesFileName);
    }
    os::remove(whFile);

    output->info(fmt::format("Restored \"{0}\"", fileName));

    return true;
}
