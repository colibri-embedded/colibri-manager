/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file boot.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <algorithm>
#include <colibrimngr/config.h>
#include <fstream>
#include <handler/boot.hpp>
#include <helper/os_helpers.hpp>
#include <helper/string_helpers.hpp>
#include <iostream>
#include <string>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

#include <network/endpoints.h>

using namespace ce::handler;
using namespace helper;

Boot::Boot(std::shared_ptr<logger::Base> logger)
  : Base("boot", logger)
  , boot_dir(DEFAULT_BOOT_DIR)
  , fn_regex("([0-9]+)-([a-zA-Z0-9-]+)-v([0-9.]+)(\\.zip)?")
{
    valid = true;

    addSupportedCommand("installed");
    addSupportedCommand("install");

    addSupportedType("os.boot");
    addSupportedArch("armhf");
}

void Boot::setBaseDirs(const std::string& basedir)
{
    boot_dir = basedir;
}

bool Boot::install(const std::string& item_name,
                   const std::string& item_arch,
                   const std::string& item_version,
                   const std::string& item_filename,
                   bool force)
{
    auto installed = this->installed(item_arch);
    std::string boot_version;
    std::string name;
    std::string version;
    unsigned priority;

    std::string filename = os::baseName(item_filename);
    if (!splitFilename(filename, priority, name, version)) {
        output->error(fmt::format("Cannot parse filename \"std::shared_ptr<output::Base> _output{0}\"", filename));
        return false;
    }
    boot_version = fmt::format("{0:03}-{1}-v{2}", priority, name, version);

    remountRW();
    std::string cmd = fmt::format("unzip -o {0} -d {1}", item_filename, boot_dir);
    if (not os::executeSimple(cmd)) {
        output->error(fmt::format("Failed to extract bootfile package \"{0}\"", item_name));
        remountRO();
        return false;
    }

    // TODO:update boot_version
    auto versionFile = os::pathJoin({ boot_dir, "boot_version" });
    output->debug(fmt::format("Write \"{0}\" to \"{1}\"", boot_version, versionFile));

    boot_version += "\n";
    if (not os::createFile(versionFile, boot_version, true)) {
        output->error("Failed to update \"boot_version\" file.");
        remountRO();
        return false;
    }

    auto cmdlineTxtFile = os::pathJoin({ boot_dir, "cmdline.txt" });
    cmd                 = fmt::format("sed -i {0} -e 's/colibri.firstboot=1/colibri.firstboot=0/'", cmdlineTxtFile);
    if (not os::executeSimple(cmd)) {
        output->error(fmt::format("Failed to fix cmdline.txt and disable firstboot."));
        remountRO();
        return false;
    }
    remountRO();

    return true;
}

/**
 * Get info about intalled boot package
 */
std::shared_ptr<std::vector<Installed>> Boot::installed(const std::string& item_arch, bool only_active)
{
    if (!(bool)_installed) {
        _installed = std::make_shared<std::vector<Installed>>();
    } else {
        output->debug(fmt::format("Installed is cached."));
        return _installed;
    }

    auto versionFile = os::pathJoin({ boot_dir, "boot_version" });
    if (os::exists(versionFile)) {
        std::ifstream infile(versionFile, std::ifstream::in);
        while (infile) {
            std::string line;
            std::getline(infile, line);
            line = str::trim(line);
            output->debug(fmt::format("boot_version Line: {0}", line));
            if (!line.empty()) {

                unsigned priority;
                std::string name;
                std::string version;

                if (splitFilename(line, priority, name, version)) {
                    _installed->push_back(Installed(name, "armhf", version, priority));
                } else {
                    output->error(fmt::format("Failed parsing line from boot_version file"));
                }
            }
        }
        infile.close();
    }

    return _installed;
}

bool Boot::splitFilename(std::string& fileName, unsigned& priority, std::string& name, std::string& version) const
{
    std::smatch match;
    if (std::regex_match(fileName, match, fn_regex)) {
        if (match.size() == 4 || match.size() == 5) {
            std::string tmp = match[1];
            priority        = std::atoi(tmp.c_str());
            name            = match[2];
            version         = match[3];
            return true;
        }
    }
    return false;
}

bool Boot::remountRW() const
{
    unsigned exitCode = -1;
    std::string cmd   = "mount -o remount,rw " + boot_dir;
    os::execute(cmd, &exitCode);
    return exitCode == 0;
}

bool Boot::remountRO() const
{
    unsigned exitCode = -1;
    std::string cmd   = "mount -o remount,ro " + boot_dir;
    os::execute(cmd, &exitCode);
    return exitCode == 0;
}

bool Boot::pre_install_steps(const std::vector<task::sptrTask>& tasks) const
{
    // Clear boot_version file content
    bool have_boot_items    = false;
    bool have_install_tasks = false;

    for (auto& task : tasks) {
        if (task->getHandler() == nullptr)
            continue;
        if (task->getHandler()->name() == this->name())
            have_boot_items = true;
        if (task->getType() == task::Type::INSTALL)
            have_install_tasks = true;
    }

    if (!have_boot_items || !have_install_tasks) {
        return true;
    }

    auto versionFile       = os::pathJoin({ boot_dir, "boot_version" });
    auto versionBackupFile = versionFile + ".bak";
    output->debug(fmt::format("Clear boot_versions @ \"{0}\"", versionFile));

    remountRW();
    if (!os::copyFile(versionFile, versionBackupFile)) {
        remountRO();
        output->error("Failed to create boot_version backup");
        return false;
    }

    if (!os::createFile(versionFile, "")) {
        output->error(fmt::format("Failed to clear \"boot_version\" file."));
        remountRO();
        return false;
    }
    remountRO();
    return true;
}

bool Boot::plan_install(std::vector<task::sptrTask>& tasks, std::shared_ptr<cache::Cache> cache)
{
    // add all installed boot items to the list
    bool have_boot_items    = false;
    bool have_install_tasks = false;

    for (auto& task : tasks) {
        if (task->getHandler()->name() == this->name())
            have_boot_items = true;
        if (task->getType() == task::Type::INSTALL)
            have_install_tasks = true;
    }

    if (!have_boot_items || !have_install_tasks) {
        return true;
    }

    // TODO: detect arch based on install task items

    auto installed_bootfiles = installed("*"); // TODO: fix for multi-arch colibri-linux support
    for (auto& item : *installed_bootfiles) {
        bool dont_have_item = true;
        for (auto& titem : tasks) {
            if (titem->getHandler()->name() == this->name()) {
                if (titem->getItem().name == item.name) {
                    dont_have_item = false;
                    break;
                }
            }
        }
        if (dont_have_item) {
            auto online = cache->getLatest(item.name, "os.boot", "*"); // TODO: fix for multi-arch colibri-linux support
            if ((bool)online) {
                auto handler = std::shared_ptr<handler::Base>(this);
                auto dt      = std::make_shared<task::Task>(
                    handler, ce::task::Type::DOWNLOAD, online->name, "os.boot", online->latest, online->priority);
                auto di = std::make_shared<task::Task>(
                    handler, ce::task::Type::INSTALL, online->name, "os.boot", online->latest, online->priority);
                dt->setDependee(di);
                dt->setFileName(online->fileName);
                dt->setFileUrl(online->repo->url + DOWNLOAD_FILE + "/" + online->fileHashId);
                dt->setChecksum(online->fileChecksum);
                dt->setChecksumAlgorithm(static_cast<unsigned>(online->alg));

                tasks.push_back(dt);
                tasks.push_back(di);
            } else {
                output->error(fmt::format("Cannot find \"{}\" in the online repository", item.name));
                return false;
            }
        }
    }

    return true;
}
