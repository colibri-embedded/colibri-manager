/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file base.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <handler/base.hpp>
using namespace ce::handler;

Base::Base(const std::string& __name, std::shared_ptr<logger::Base> _output)
  : _name(__name)
  , valid(false)
  , output(_output)
{}

std::string Base::name() const
{
    return _name;
}

void Base::addSupportedCommand(const std::string& cmd)
{
    commands.insert(cmd);
}

bool Base::supportsCommand(const std::string& cmd) const
{
    auto found = commands.find(cmd);
    return found != commands.end();
}

void Base::addSupportedType(const std::string& type)
{
    types.insert(type);
}

bool Base::supportsType(const std::string& type) const
{
    auto found = types.find(type);
    return found != types.end();
}

void Base::addSupportedArch(const std::string& arch)
{
    architectures.insert(arch);
}

bool Base::supportsArch(const std::string& arch) const
{
    if (arch == "any")
        return true;
    auto found = architectures.find(arch);
    return found != architectures.end();
}

bool Base::isValid() const
{
    return valid;
}

bool Base::activate(const std::string& item_name, const std::string& item_arch)
{
    return false;
}

bool Base::deactivate(const std::string& item_name, const std::string& item_arch)
{
    return false;
}

bool Base::enable(const std::string& item_name, const std::string& item_arch)
{
    return false;
}

bool Base::disable(const std::string& item_name, const std::string& item_arch)
{
    return false;
}

bool Base::remove(const std::string& item_name, const std::string& item_arch, bool force)
{
    return false;
}

bool Base::restore(const std::string& item_name, const std::string& item_arch, bool force)
{
    return false;
}

bool Base::splitFilename(std::string& fileName, unsigned& priority, std::string& name, std::string& version) const
{
    return false;
}

bool Base::install(const std::string& item_name,
                   const std::string& item_arch,
                   const std::string& item_version,
                   const std::string& item_filename,
                   bool force)
{
    return false;
}

std::shared_ptr<std::vector<Installed>> Base::installed(const std::string& item_arch, bool only_active)
{
    if (!(bool)_installed) {
        _installed = std::make_shared<std::vector<Installed>>();
    }
    return _installed;
}

void Base::clear_cached()
{
    _installed.reset();
}

void Base::onboot() {}

bool Base::pre_install_steps(const std::vector<task::sptrTask>& tasks) const
{
    return true;
}

bool Base::plan_install(std::vector<task::sptrTask>& tasks, std::shared_ptr<cache::Cache> cache)
{
    return true;
}