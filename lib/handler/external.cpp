/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file external.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <stdlib.h>
#include <string>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

#include <handler/external.hpp>
#include <helper/string_helpers.hpp>

using namespace ce::handler;
using namespace helper;

External::External(const std::string& name, const std::string& _script, std::shared_ptr<logger::Base> logger)
  : Base(name, logger)
  , script(_script)
{
    valid = this->checkScript();
}

/**
 * Execute external bash script with provided arguments and return the output.
 *
 * @param  args Arguments string to pass to the external script
 * @param  exitCode Bash script exit code
 * @return      Script output
 */
std::string External::execute(const std::string& args, unsigned* exitCode) const
{
    std::array<char, 400> buffer;
    std::string result;
    std::string cmd = "sh " + script + " " + args;
    // std::shared_ptr<FILE> pipe( popen(cmd.c_str(), "r"), pclose);
    FILE* pipe = popen(cmd.c_str(), "r");

    if (!pipe)
        throw std::runtime_error(fmt::format(cmd + " failed!"));

    while (!feof(pipe)) {
        if (fgets(buffer.data(), 128, pipe) != nullptr)
            result += buffer.data();
    }

    auto returnCode = pclose(pipe);

    if (exitCode != nullptr) {
        if (WIFEXITED(returnCode)) {
            *exitCode = WEXITSTATUS(returnCode);
        }
    }

    return result;
}

/**
 * Check if the external script is working and has the minimal supported commands.
 */
bool External::checkScript()
{
    try {
        auto script_output = this->execute("supported");
        auto supported     = str::split(script_output, ',');

        // read supported commands
        for (auto& cando : supported) {
            // std::cout << "can: " << cando << "\n";
            this->addSupportedCommand(str::trim(cando));
        }

        if (!this->supportsCommand("installed")) {
            output->error(fmt::format("installed command missing for {0}", this->script));
            return false;
        }

        if (!this->supportsCommand("install")) {
            output->error(fmt::format("install command missing for {0}", this->script));
            return false;
        }

        // this->_arch   = this->execute("arch");
        script_output = this->execute("handles");
        auto types    = str::split(script_output, ',');

        if (types.size() < 1)
            return false;

        for (auto& tpe : types) {
            // trim space and new line characters
            this->addSupportedType(str::trim(tpe));
        }

        script_output      = this->execute("arch");
        auto architectures = str::split(script_output, ',');

        if (architectures.size() < 1)
            return false;

        for (auto& a : architectures) {
            // trim space and new line characters
            this->addSupportedArch(str::trim(a));
        }

        return true;
    } catch (std::exception& e) {
        output->error(fmt::format(e.what()));
        return false;
    }
}

bool External::install(const std::string& item_name,
                       const std::string& item_arch,
                       const std::string& item_version,
                       const std::string& item_filename,
                       bool force)
{
    if (!supportsCommand("install"))
        return false;

    std::string args = fmt::format("install {0} {1} {2} {3}", item_name, item_arch, item_version, item_filename);
    if (force) {
        args += " --force";
    }
    unsigned exitCode = EXIT_FAILURE;
    auto output       = this->execute(args, &exitCode);
    std::cout << output;

    return exitCode == EXIT_SUCCESS;
}

bool External::remove(const std::string& item_name, const std::string& item_arch, bool force)
{
    if (!supportsCommand("remove"))
        return false;

    std::string args = fmt::format("remove {0} {1}", item_name, item_arch);
    if (force) {
        args += " --force";
    }
    unsigned exitCode = EXIT_FAILURE;
    auto output       = this->execute(args, &exitCode);
    std::cout << output;

    return exitCode == EXIT_SUCCESS;
}

bool External::restore(const std::string& item_name, const std::string& item_arch, bool force)
{
    if (!supportsCommand("restore"))
        return false;

    std::string args = fmt::format("restore {0} {1}", item_name, item_arch);
    if (force) {
        args += " --force";
    }
    unsigned exitCode = EXIT_FAILURE;
    auto output       = this->execute(args, &exitCode);
    std::cout << output;

    return exitCode == EXIT_SUCCESS;
}

bool External::activate(const std::string& item_name, const std::string& item_arch)
{
    if (!supportsCommand("activate"))
        return false;

    std::string args  = fmt::format("activate {0} {1}", item_name, item_arch);
    unsigned exitCode = EXIT_FAILURE;
    auto output       = this->execute(args, &exitCode);
    std::cout << output;

    return exitCode == EXIT_SUCCESS;
}

bool External::deactivate(const std::string& item_name, const std::string& item_arch)
{
    if (!supportsCommand("deactivate"))
        return false;

    std::string args  = fmt::format("deactivate {0} {1}", item_name, item_arch);
    unsigned exitCode = EXIT_FAILURE;
    auto output       = this->execute(args, &exitCode);
    std::cout << output;

    return exitCode == EXIT_SUCCESS;
}

std::shared_ptr<std::vector<Installed>> External::installed(const std::string& item_arch, bool only_active)
{
    // auto result = std::unique_ptr< std::vector<Installed> >(new std::vector<Installed>);
    if (!bool(_installed)) {
        _installed = std::make_shared<std::vector<Installed>>();
    } else {
        return _installed;
    }

    if (!supportsCommand("installed"))
        return _installed;

    std::string args   = fmt::format("installed {0}", item_arch);
    auto script_output = this->execute(args);
    auto items         = str::split(script_output, '\n');
    for (auto& item : items) {
        auto tags = str::split(item, ':');
        if (tags.size() != 3) {
            output->error(fmt::format("\"installed\" output format error: {0}", item));
            return _installed;
        }
        _installed->push_back(
            Installed(str::trim(tags[0]), str::trim(tags[1]), str::trim(tags[2]), 0 /*priority*/, INSTALLED | ACTIVE));
    }

    return _installed;
}

bool External::splitFilename(std::string& fileName, unsigned& priority, std::string& name, std::string& version) const
{
    if (!supportsCommand("tokenize"))
        return false;

    std::string args  = "tokenize " + fileName;
    unsigned exitCode = EXIT_FAILURE;
    auto output       = this->execute(args, &exitCode);

    if (exitCode != EXIT_SUCCESS)
        return false;

    auto lines = str::split(output, '\n');
    for (auto& line : lines) {
        auto tags = str::split(line, ':');
        if (tags.size() != 2)
            return false;

        if (tags[0] == "priority") {
            priority = std::stoi(tags[1]);
        } else if (tags[0] == "name") {
            name = str::trim(tags[1]);
        } else if (tags[0] == "version") {
            version = str::trim(tags[1]);
        }
    }

    return true;
}

void External::onboot()
{
    if (!supportsCommand("onboot"))
        return;

    std::string args = "onboot";
    this->execute(args);
}
