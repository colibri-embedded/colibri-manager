/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file sync.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <handler/base.hpp>
#include <helper/os_helpers.hpp>
#include <helper/string_helpers.hpp>
#include <network/endpoints.h>
#include <network/graphql.h>
#include <network/http_client.hpp>
#include <task/sync.hpp>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace helper;
using namespace ce;
using namespace ce::task;

Sync::Sync(std::shared_ptr<cache::Cache>& cache, const std::string& cache_file)
  : Task(nullptr, Type::SYNC, "", "", "")
  , m_cache(cache)
  , m_cache_file(cache_file)
{}

bool Sync::execute(const std::shared_ptr<logger::Base>& logger, const std::string& tmp_dir)
{
    auto client = std::make_unique<ce::network::HttpClient>();

    logger->debug(fmt::format("ManagerImpl::sync"));
    m_cache->clearRepositories();

    client->progressCallback = [this](unsigned long total, unsigned long current) {
        this->setTotalProgress(total);
        this->setProgress(current);
    };

    const auto sources = m_cache->getSources();
    m_cache->clearRepositories();
    m_cache->setLastSyncToNow();
    // unsigned current = 0;
    // this->setTotalProgress(sources.size());
    // this->setProgress(current);
    for (auto& source : sources) {
        logger->debug(fmt::format("ManagerImpl::sync source: \"{0}\"", source->toString()));
        logger->info(fmt::format("Fetching {0} {1}", source->name, source->toString()));

        std::string branches = str::join(source->branches, ",", "\\\"", "\\\"");
        auto query           = fmt::format(QUERY_VERSIONS, source->key, branches);

        client->setBaseUrl(source->url);
        auto result = client->gql_query(GRAPHQL_PUBLIC, query);

        logger->debug(fmt::format("== server response start ==\n {0}\n== server response end ==", result));

        try {
            auto js     = json::parse(result);
            auto errors = js["errors"];

            if (errors == nullptr) {
                // seems ok, lets parse it
                auto data = js["data"]["latestVersions"];
                if (data != nullptr) {
                    logger->debug("update cache");
                    m_cache->update(data, source->url, source->branches);
                }
            } else {
                // error handling
                logger->error(fmt::format("Server error"));
                return false;
            }
        } catch (const nlohmann::detail::parse_error& e) {
            logger->error(fmt::format("Error parsing server response"));
            logger->debug(fmt::format("  {0}", e.what()));
            return false;
        }

        // this->setProgress(++current);
    }
    m_cache->store(m_cache_file);

    return true;
}
