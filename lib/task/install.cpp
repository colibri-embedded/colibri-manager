/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file install.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <handler/base.hpp>
#include <helper/os_helpers.hpp>
#include <task/install.hpp>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace helper;
using namespace ce;
using namespace ce::task;

Install::Install(const std::shared_ptr<handler::Base>& handler,
                 const std::string& item_name,
                 const std::string& item_type,
                 const std::string& item_arch,
                 const std::string& item_version,
                 unsigned priority,
                 bool forced)
  : Task(handler, Type::INSTALL, item_name, item_type, item_arch, item_version, priority, false /* is_local */, forced)
{}

Install::Install(const std::shared_ptr<handler::Base>& handler,
                 const std::string& item_name,
                 const std::string& item_type,
                 const std::string& item_arch,
                 const std::string& item_version,
                 bool forced)
  : Task(handler, Type::INSTALL, item_name, item_type, item_arch, item_version, 0, true /* is_local*/, forced)
{}

Install::Install(const std::shared_ptr<handler::Base>& handler, const task::Request& request)
  : Task(handler, request)
{}

bool Install::execute(const std::shared_ptr<logger::Base>& logger, const std::string& tmp_dir)
{
    try {

        auto handler = getHandler();
        auto item    = getItem();

        if (!os::mkDir(tmp_dir, true)) {
            throw std::runtime_error(fmt::format("Cannot create temporary directory \"{0}", tmp_dir));
        }

        if (isLocal()) {
            // Install item from local file
            unsigned priority = 0;
            std::string name;
            std::string version;
            auto filename = os::baseName(getFileName());

            // logger->info( fmt::format("Install local file: {0}", filename) );
            if (!os::exists(filename))
                throw std::runtime_error(fmt::format("{0}: file does not exist", filename));

            if (handler->splitFilename(filename, priority, name, version)) {
                // logger->info( fmt::format("{0}: Installing {1}", item.name, version) );
                if (!handler->install(name, item.arch, version, getFileName(), isForced())) {
                    throw std::runtime_error(fmt::format("{0}: Failed", item.name));
                }

            } else {
                throw std::runtime_error(fmt::format("{0}: Failed to parse filename", item.name));
            }
        } else {
            // logger->info( fmt::format("{0}: Installing {1}", item.name, item.version) );
            if (!handler->install(item.name, item.arch, item.version, getFileName(), isForced())) {
                throw std::runtime_error(fmt::format("{0}: Filed", item.name));
            }
        }

        if (!isForced() && (item.type == "os.bundles" || item.type == "os.boot")) {
            if (m_callbacks.reboot_required) {
                m_callbacks.reboot_required(true);
            }
        }
    } catch (std::exception& e) {
        logger->error(e.what());
        return false;
    }

    return true;
}