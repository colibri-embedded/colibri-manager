/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file activate.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <handler/base.hpp>
#include <helper/os_helpers.hpp>
#include <task/activate.hpp>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace helper;
using namespace ce;
using namespace ce::task;

Activate::Activate(const std::shared_ptr<handler::Base>& handler,
                   const std::string& item_name,
                   const std::string& item_type,
                   const std::string& item_arch)
  : Task(handler, Type::ACTIVATE, item_name, item_type, item_arch)
{
    using namespace std::placeholders;
    setExecuteCallback(std::bind(&Activate::execute, this, _1, _2));
}

Activate::Activate(const std::shared_ptr<handler::Base>& handler, const task::Request& request)
  : Task(handler, request)
{}

bool Activate::execute(const std::shared_ptr<logger::Base>& logger, const std::string& tmp_dir)
{
    auto handler = getHandler();
    auto item    = getItem();

    return handler->activate(item.name, item.arch);
}