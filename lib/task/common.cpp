/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file task.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#include <task/task.hpp>
#include <handler/base.hpp>

#include <checksum/md5.hpp>
#include <checksum/sha1.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace ce::task;

std::string ce::task::toString(Status s)
{
    switch(s) {
        case Status::IDLE:
            return "idle";
        case Status::WORKING:
            return "working";
        case Status::DONE:
            return "done";
        case Status::FAILED:
            return "failed";
        case Status::ABORTED:
            return "aborted";
        default:
            return "";
    }
}

std::string ce::task::toString(Type t)
{
    switch(t) {
        case Type::NONE:
            return "none";
        case Type::CUSTOM:
            return "custom";
        case Type::SYNC:
            return "sync";
        case Type::DOWNLOAD:
            return "download";
        case Type::REMOVE:
            return "remove";
        case Type::INSTALL:
            return "install";
        case Type::UPDATE:
            return "update";
        case Type::RESTORE:
            return "restore";
        case Type::DEACTIVATE:
            return "deactivate";
        case Type::ACTIVATE:
            return "activate";
        default:
            return "";
    }
}