/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file task.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <handler/base.hpp>
#include <task/task.hpp>

#include <checksum/md5.hpp>
#include <checksum/sha1.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace ce;
using namespace ce::task;

Task::Task(const std::shared_ptr<handler::Base>& handler,
           task::Type _type,
           const std::string& item_name,
           const std::string& item_type,
           const std::string& item_arch,
           const std::string& item_version,
           unsigned item_priority,
           bool local,
           bool forced)
  : m_handler(handler)
  , m_type(_type)
  , m_status(task::Status::IDLE)
  , m_item{ item_name, item_type, item_arch, item_version, item_priority }
  , m_forced(forced)
  , m_local(local)
  , m_total_progress(0)
  , m_current_progress(0)
{}

Task::Task(const std::shared_ptr<handler::Base>& handler,
           task::Type _type,
           const std::string& item_name,
           const std::string& item_type,
           const std::string& item_arch,
           bool forced,
           bool local)
  : m_handler(handler)
  , m_type(_type)
  , m_status(task::Status::IDLE)
  , m_item{ item_name, item_type, item_arch }
  , m_forced(forced)
  , m_local(local)
  , m_total_progress(0)
  , m_current_progress(0)
{}

Task::Task(const std::shared_ptr<handler::Base>& handler, const task::Request& request)
  : m_handler(handler)
  , m_type(request.operation)
  , m_status(task::Status::IDLE)
  , m_item(request.item)
  , m_forced(request.is_forced)
  , m_local(request.is_local)
  , m_total_progress(0)
  , m_current_progress(0)
{}

void Task::setId(unsigned value)
{
    m_id = value;
}

unsigned Task::getId() const
{
    return m_id;
}

Callbacks Task::getCallbacks()
{
    return m_callbacks;
}

void Task::setCallbacks(Callbacks callbacks)
{
    m_callbacks = callbacks;
}

std::shared_ptr<Task> Task::getDependee()
{
    return m_dependee;
}

void Task::setDependee(const std::shared_ptr<Task>& value)
{
    m_dependee = value;
}

const std::shared_ptr<handler::Base>& Task::Task::getHandler() const
{
    return m_handler;
}

std::shared_ptr<handler::Base>& Task::Task::getHandler()
{
    return m_handler;
}

const task::Item& Task::getItem() const
{
    return m_item;
}

task::Status Task::getStatus() const
{
    return m_status;
}

void Task::setStatus(task::Status value)
{
    bool changed = (m_status != value);
    m_status     = value;
    if (changed)
        task_changed();
}

task::Type Task::getType() const
{
    return m_type;
}

bool Task::isForced() const
{
    return m_forced;
}

void Task::setForced(bool value)
{
    m_forced = value;
}

void Task::setLocal(bool value)
{
    m_local = value;
}

bool Task::isLocal() const
{
    return m_local;
}

const std::string& Task::getFileName() const
{
    return m_filename;
}

void Task::setFileName(const std::string& value)
{
    m_filename = value;
}

const std::string& Task::getFileUrl() const
{
    return m_file_url;
}

void Task::setFileUrl(const std::string& value)
{
    m_file_url = value;
}

unsigned long Task::getProgress() const
{
    return m_current_progress;
}

void Task::setProgress(unsigned long value)
{
    auto changed       = (m_current_progress != value);
    m_current_progress = value;
    if (changed)
        task_changed();
}

unsigned long Task::getTotalProgress() const
{
    return m_total_progress;
}

void Task::setTotalProgress(unsigned long value)
{
    bool changed     = (m_total_progress != value);
    m_total_progress = value;
    if (changed)
        task_changed();
}

void Task::setChecksum(const std::string& value)
{
    m_checksum = value;
}

const std::string& Task::getChecksum() const
{
    return m_checksum;
}

void Task::setChecksumAlgorithm(unsigned value)
{
    m_checksum_alg = value;
}

unsigned Task::getChecksumAlgorithm() const
{
    return m_checksum_alg;
}

void Task::setExecuteCallback(task_execute_cb callback)
{
    m_execute = callback;
}

bool Task::execute(const std::shared_ptr<logger::Base>& logger, const std::string& tmp_dir)
{
    if (m_execute) {
        return m_execute(logger, tmp_dir);
    }
    return true;
}

bool Task::validateFileChecksum(const std::string& filename)
{
    bool valid = false;
    if (getChecksumAlgorithm() == static_cast<unsigned>(ce::cache::Algorithm::MD5)) {
        auto checksum = MD5::from_file(filename);
        valid         = (checksum == getChecksum());
    } else if (getChecksumAlgorithm() == static_cast<unsigned>(ce::cache::Algorithm::SHA1)) {
        auto checksum = SHA1::from_file(filename);
        valid         = (checksum == getChecksum());
    } else {
        throw std::runtime_error(fmt::format("Unsupported checksum algorithm"));
    }

    return valid;
}

void Task::task_changed()
{
    if (m_callbacks.task_changed) {
        m_callbacks.task_changed(this);
    }
}