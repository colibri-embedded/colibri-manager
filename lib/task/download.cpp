/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file download.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <handler/base.hpp>
#include <helper/os_helpers.hpp>
#include <network/endpoints.h>
#include <network/http_client.hpp>
#include <task/download.hpp>
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace helper;
using namespace ce;
using namespace ce::task;

Download::Download(const std::shared_ptr<Task> dep,
                   const std::string& filename,
                   const std::string& file_url,
                   const std::string& checksum,
                   unsigned checksum_alg)
  : Task(dep->getHandler(),
         Type::DOWNLOAD,
         dep->getItem().name,
         dep->getItem().type,
         dep->getItem().arch,
         dep->getItem().version,
         dep->getItem().priority,
         false, /* is_local */
         false  /* is_forced */
    )
{
    setDependee(dep);
    setFileName(filename);
    setFileUrl(file_url);
    setChecksum(checksum);
    setChecksumAlgorithm(checksum_alg);
}

bool Download::execute(const std::shared_ptr<logger::Base>& logger, const std::string& tmp_dir)
{
    try {

        auto item   = getItem();
        auto client = std::make_unique<network::HttpClient>();

        if (!os::mkDir(tmp_dir, true)) {
            throw std::runtime_error(fmt::format("Cannot create temporary directory \"{0}", tmp_dir));
        }

        std::string tmp_file = os::pathJoin({ tmp_dir, getFileName() });
        // logger->info( fmt::format("Downloading: {0} -> {1}", getFileUrl(), tmp_file) );

        client->progressCallback = [this](unsigned long total, unsigned long current) {
            this->setTotalProgress(total);
            this->setProgress(current);
        };
        // client->finishedCallback = [this]() {
        // std::cout << "Download finished\n";
        // };

        bool have_valid_file = false;

        if (os::exists(tmp_file)) {
            have_valid_file = validateFileChecksum(tmp_file);
        }

        if (!have_valid_file) {
            if (client->downloadFile(getFileUrl(), tmp_file)) {
                bool valid = validateFileChecksum(tmp_file);

                if (!valid) {
                    throw std::runtime_error(
                        fmt::format("File checksum for \"{0}\" is wrong should be \"{1}\"", tmp_file, getChecksum()));
                }
            } else {
                throw std::runtime_error(fmt::format("Failed to download file for \"{0}\"", item.name));
            }
        }

        auto dep = getDependee();
        if ((bool)dep) {
            dep->setFileName(tmp_file);
        }
    } catch (std::exception& e) {
        logger->error(e.what());
        return false;
    }

    return true;
}