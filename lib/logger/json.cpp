#include <logger/json.hpp>
#include <handler/base.hpp>

using namespace ce::logger;
using json = nlohmann::json;

Json::Json(unsigned _log_level)
    : Base("json", _log_level)
{

}

void Json::debug(const std::string& message)
{
    if(log_level > LogLevel::INFO)
        messages.push_back({LogLevel::DEBUG, message});
}

void Json::info(const std::string& message)
{
    if(log_level >= LogLevel::INFO)
        messages.push_back({LogLevel::INFO, message});
}

void Json::warn(const std::string& message)
{
    if(log_level >= LogLevel::WARN)
        messages.push_back({LogLevel::WARN, message});
}

void Json::error(const std::string& message)
{
    if(log_level >= LogLevel::ERROR)
        messages.push_back({LogLevel::ERROR, message});
}

void Json::critical(const std::string& message)
{
    if(log_level >= LogLevel::CRITICAL)
        messages.push_back({LogLevel::CRITICAL, message});
}

void Json::item(
    const std::string& item_name, 
    const std::string& item_type,
    const std::string& local_version,
    const std::string& online_version,
    const std::string& changelog,
    unsigned state
) {
    items.push_back({
        {"name", item_name},
        {"type", item_type},
        {"local_version", local_version},
        {"online_version", online_version},
        {"changelog", changelog},
        {"state", {
            {"installed", (bool)(state & handler::INSTALLED) },
            {"active", (bool)(state & handler::ACTIVE) },
            {"online", (bool)(state & handler::ONLINE) },
            {"optional", (bool)(state & handler::OPTIONAL) },
        }}
    });
}

void Json::search_result(
    const std::string& item_name, 
    const std::string& item_type,
    const std::string& item_branch,
    const std::string& online_version,
    const std::string& local_version,
    unsigned state
) {

    items.push_back({
        {"name", item_name},
        {"type", item_type},
        {"branch", item_type},
        {"online_version", online_version},
        {"local_version", local_version},
        {"state", {
            {"installed", (bool)(state & handler::INSTALLED) },
            {"active", (bool)(state & handler::ACTIVE) },
            {"online", (bool)(state & handler::ONLINE) },
            {"optional", (bool)(state & handler::OPTIONAL) },
        }}
    });
}

std::string Json::toString() const
{
    json debug_list = json::array();
    json info_list = json::array();
    json warn_list = json::array();
    json error_list = json::array();
    json crit_list = json::array();

    for(auto &msg: messages) {
        switch(msg.level) {
            case LogLevel::DEBUG:
                debug_list.push_back(msg.text);
                break;
            case LogLevel::INFO:
                info_list.push_back(msg.text);
                break;
            case LogLevel::WARN:
                warn_list.push_back(msg.text);
                break;
            case LogLevel::ERROR:
                error_list.push_back(msg.text);
                break;
            case LogLevel::CRITICAL:
                crit_list.push_back(msg.text);
                break;
        }
    }

    json obj = {
        {"items", items},
        {"debug", debug_list},
        {"info", info_list},
        {"warn", warn_list},
        {"error", error_list},
        {"critical", crit_list},
    };

    return obj.dump(4);
}