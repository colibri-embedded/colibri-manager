#include <logger/stdio.hpp>
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <iostream>
#include <handler/base.hpp>

using namespace ce::logger;

Stdio::Stdio(unsigned _log_level)
    : Base("stdio", _log_level)
{
}

void Stdio::debug(const std::string& message)
{
	if(log_level >= LogLevel::DEBUG) {
        std::cout << fmt::format("[DEBUG] {0}\n", message);
	}
}

void Stdio::info(const std::string& message)
{
	if(log_level >= LogLevel::INFO) {
		std::cout << fmt::format("{0}\n", message);
	}
}

void Stdio::warn(const std::string& message)
{
	if(log_level >= LogLevel::WARN) {
		std::cout << fmt::format("[WARN] {0}\n", message);
	}
}

void Stdio::error(const std::string& message)
{
	if(log_level >= LogLevel::ERROR) {
		std::cout << fmt::format("[ERROR] {0}\n", message);
	}
}

void Stdio::critical(const std::string& message)
{
	if(log_level >= LogLevel::CRITICAL) {
		std::cout << fmt::format("[CRIT] {0}\n", message);
	}
}

void Stdio::item(
    const std::string& item_name, 
    const std::string& item_type,
    const std::string& local_version,
    const std::string& online_version,
    const std::string& changelog,
    unsigned state
) {
    std::string sstate = "---";

    if(state & ce::handler::ACTIVE)
        sstate[0] = 'a';

    if(state & ce::handler::INSTALLED)
        sstate[1] = 'i';

    if(state & ce::handler::ONLINE)
        sstate[2] = 'o';

    if(online_version.empty()) {
        std::cout << fmt::format("{0} {1} - {2}\n",
            sstate,
            item_name,
            local_version);
    } else {
        std::cout << fmt::format("{0} {1} - {2} [{3}]\n",
            sstate,
            item_name,
            local_version,
            online_version);
    }
}

void Stdio::search_result(
    const std::string& item_name, 
    const std::string& item_type,
    const std::string& item_branch,
    const std::string& online_version,
    const std::string& local_version,
    unsigned state
) {

    std::string sstate = "---";

    if(state & ce::handler::ACTIVE)
        sstate[0] = 'a';

    if(state & ce::handler::INSTALLED)
        sstate[1] = 'i';

    if(state & ce::handler::ONLINE)
        sstate[2] = 'o';

    if(local_version.empty()) {
        std::cout << fmt::format("{0} {1} - {2}\n",
            sstate,
            item_name,
            online_version);
    } else {
        std::cout << fmt::format("{0} {1} - {2} [{3}]\n",
            sstate,
            item_name,
            online_version,
            local_version);
    }
}