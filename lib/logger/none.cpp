#include <logger/none.hpp>

using namespace ce::logger;

None::None(unsigned _log_level)
    : Base("none", _log_level)
{}

void None::debug(const std::string& message) {}

void None::info(const std::string& message) {}

void None::warn(const std::string& message) {}

void None::error(const std::string& message) {}

void None::critical(const std::string& message) {}

void None::item(
    const std::string& item_name, 
    const std::string& item_type,
    const std::string& local_version,
    const std::string& online_version,
    const std::string& changelog,
    unsigned state
) {}

void None::search_result(
    const std::string& item_name, 
    const std::string& item_type,
    const std::string& item_branch,
    const std::string& online_version,
    const std::string& local_version,
    unsigned state
) {}