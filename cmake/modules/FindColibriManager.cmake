# This module tries to find libcolibrimngr library and include files
#
# COLIBRI_MANAGER_INCLUDE_DIR, path where to find colibrimgr/manager.hpp
# COLIBRI_MANAGER_LIBRARY_DIR, path where to find libcolibrimngr.so
# COLIBRI_MANAGER_LIBRARIES, the library to link against
# COLIBRI_MANAGER_FOUND, If false, do not try to use libcolibrimngr
#
# This currently works probably only for Linux

FIND_PATH ( COLIBRI_MANAGER_INCLUDE_DIR colibrimngr/manager.hpp
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( COLIBRI_MANAGER_LIBRARIES ColibriManager
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( COLIBRI_MANAGER_LIBRARY_DIR ${COLIBRI_MANAGER_LIBRARIES} PATH )

SET ( COLIBRI_MANAGER_FOUND "NO" )
IF ( COLIBRI_MANAGER_INCLUDE_DIR )
    IF ( COLIBRI_MANAGER_LIBRARIES )
        SET ( COLIBRI_MANAGER_FOUND "YES" )
    ENDIF ( COLIBRI_MANAGER_LIBRARIES )
ENDIF ( COLIBRI_MANAGER_INCLUDE_DIR )

MARK_AS_ADVANCED(
    COLIBRI_MANAGER_LIBRARY_DIR
    COLIBRI_MANAGER_INCLUDE_DIR
    COLIBRI_MANAGER_LIBRARIES
)
