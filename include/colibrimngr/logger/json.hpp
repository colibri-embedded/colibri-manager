/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file json.hpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#ifndef COLIBRIMNGR_LOGGER_JSON_HPP
#define COLIBRIMNGR_LOGGER_JSON_HPP

#include <list>
#include <map>
#include <nlohmann/json.hpp>
#include <colibrimngr/logger/base.hpp>

namespace ce {
	namespace logger {

		struct Message {
			LogLevel level;
			std::string text;
		};

		class Json : public Base {
			public:
				Json(unsigned log_level);

				void item(
					const std::string& item_name, 
					const std::string& item_type,
					const std::string& local_version,
					const std::string& online_version,
					const std::string& changelog = "",
					unsigned state = 0
				) override;

				void search_result(
					const std::string& item_name, 
					const std::string& item_type,
					const std::string& item_branch,
					const std::string& online_version,
					const std::string& local_version = "",
					unsigned state = 0
				) override;

				void debug(const std::string& message) override;
				void info(const std::string& message) override;
				void warn(const std::string& message) override;
				void error(const std::string& message) override;
				void critical(const std::string& message) override;

                std::string toString() const override;
                
			private:

			protected:
                std::list<Message> messages;
				//std::list< std::map<std::string, std::string> > items;
				nlohmann::json items;
		};

	} /* namespace logger */
} /* namespace ce */

#endif // COLIBRIMNGR_LOGGER_JSON_HPP
