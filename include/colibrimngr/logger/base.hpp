/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file base.
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#ifndef COLIBRIMNGR_LOGGER_BASE_HPP
#define COLIBRIMNGR_LOGGER_BASE_HPP

#include <string>

namespace ce {
	namespace logger {

		enum LogLevel {
			CRITICAL = 0,
			ERROR,
			WARN,
			INFO,
			DEBUG	
		};

		class Base {
			public:
				Base(const std::string& name, unsigned log_level);

				std::string name();

				virtual void item(
					const std::string& item_name, 
					const std::string& item_type,
					const std::string& local_version,
					const std::string& online_version,
					const std::string& changelog = "",
					unsigned state = 0
				) =0;

				virtual void search_result(
					const std::string& item_name, 
					const std::string& item_type,
					const std::string& item_branch,
					const std::string& online_version,
					const std::string& local_version = "",
					unsigned state = 0
				) =0;

				virtual void debug(const std::string& message) =0;
				virtual void info(const std::string& message) =0;
				virtual void warn(const std::string& message) =0;
				virtual void error(const std::string& message) =0;
				virtual void critical(const std::string& message) =0;

				virtual std::string toString() const;
				
			private:

			protected:
				std::string _name;
				unsigned log_level;
		};

	} /* namespace logger */
} /* namespace ce */

#endif // COLIBRIMNGR_LOGGER_BASE_HPP
