/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file repository.hpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#ifndef COLIBRIMNGR_CACHE_REPOSITORY_HPP
#define COLIBRIMNGR_CACHE_REPOSITORY_HPP

#include <string>
#include <vector>
#include <memory>

#include "item.hpp"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace ce {
	namespace cache {

struct Repository {
	std::string name;
	std::string type;
	std::string url;
	std::vector<sptrItem> items;
	std::vector<std::string> branches;

	void addItem(Item* item);

	json toJson();
	bool fromJson(json &obj);

	std::string toString() const;
};

typedef std::shared_ptr<Repository> sptrRepository;
typedef std::unique_ptr<Repository> uptrRepository;

	} /* namespace cache */
} /* namespace ce */

#endif /* COLIBRIMNGR_CACHE_REPOSITORY_HPP */
