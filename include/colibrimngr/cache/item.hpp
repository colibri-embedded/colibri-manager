/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file item.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_CACHE_ITEM_HPP
#define COLIBRIMNGR_CACHE_ITEM_HPP

#include <memory>
#include <string>
#include <vector>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "version.hpp"

namespace ce {
namespace cache {

    struct Repository;

    enum class Algorithm
    {
        MD5,
        SHA1
    };

    struct Dependency
    {
        std::string name;
        std::string condition;
        std::string type;
    };

    struct Item
    {
        std::string name         = "";
        unsigned priority        = 0;
        std::string hashId       = "";
        std::string branch       = "";
        bool optional            = false;
        std::string latest       = "";
        std::string fileName     = "";
        std::string fileHashId   = "";
        std::string fileChecksum = "";
        std::string changelog    = "";

        Algorithm alg;

        Repository* repo;

        std::vector<Dependency> deps;

        json toJson();
        bool fromJson(json& obj);
    };

    typedef std::shared_ptr<Item> sptrItem;
    typedef std::unique_ptr<Item> uptrItem;

} /* namespace cache */
} /* namespace ce */

#endif /* COLIBRIMNGR_CACHE_ITEM_HPP */