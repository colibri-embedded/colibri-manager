/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file cache.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_CACHE_HPP
#define COLIBRIMNGR_CACHE_HPP

#include <chrono>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

#include "repository.hpp"
#include "source.hpp"
#include <logger/base.hpp>

using json = nlohmann::json;

namespace ce {
namespace cache {

    class Cache
    {
      public:
        explicit Cache(std::shared_ptr<logger::Base>& logger);

        bool load(const std::string& filename);
        bool store(const std::string& filename);

        void clear();
        void clearSources();
        void clearRepositories();
        sptrSource addSource(sptrSource repo);
        sptrRepository addRepository(Repository* repo);

        void update(json& obj, const std::string& url, const std::vector<std::string>& branches);

        const std::vector<sptrSource>& getSources();
        sptrSource removeSource(const std::string& name);
        sptrSource findSource(const std::string& name);

        const std::vector<sptrRepository>& getRepositories();

        std::shared_ptr<Item> getLatest(const std::string& name, const std::string& type, const std::string& arch);

        void setLastSyncToNow();
        std::chrono::system_clock::time_point getLastSyncTime();

      private:
        std::vector<sptrSource> sources;
        std::vector<sptrRepository> repositories;
        std::shared_ptr<logger::Base> m_logger;
        std::chrono::system_clock::time_point m_last_sync;

        std::string to_iso8601(std::chrono::system_clock::time_point& tp);
        std::chrono::system_clock::time_point from_iso8601(const std::string& iso);
    };

} // namespace cache
} // namespace ce

#endif /* COLIBRIMNGR_CACHE_HPP */