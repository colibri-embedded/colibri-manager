/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file os_helpers.cpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#ifndef COLIBRIMNGR_OS_HELPERS_HPP
#define COLIBRIMNGR_OS_HELPERS_HPP

#include <string>
#include <vector>
#include <memory>

typedef std::vector<std::string> Strings;
typedef std::shared_ptr<std::vector<std::string>> sptrStrings;
typedef std::unique_ptr<std::vector<std::string>> uptrStrings;

namespace helper {
	namespace os {

	uptrStrings getDirectoryFiles(const std::string& dir_name);
	bool copyFiles(const std::string& srcDir, const std::string& dstDir);
	bool copyFile(const std::string& srcFile, const std::string& dstFile);
	bool createSoftlink(const std::string& srcFile, const std::string& dstFile, bool overwrite=true);
	bool createFile(const std::string& filename, const std::string& content="", bool append=false);
	bool changeMode(const std::string& filename, unsigned mode);
	//
	bool exists(const std::string& path);
	bool remove(const std::string& path);
	bool removeDirectory(const std::string& path);
	// directory
	bool mkDir(const std::string& dirname, bool recursive=false, unsigned mode=0755);
	bool isDirectory(const std::string& path);
	bool isFile(const std::string& path);
	bool isExecutable(const std::string& path);
	// path
	std::string pathJoin(std::initializer_list<std::string> paths);
	std::string baseName(const std::string& filename);
	std::string dirName(const std::string& filename);
	std::string fileExtension(const std::string &filename);
	// process
	std::string execute(const std::string &command, unsigned *exitCode = nullptr);
	bool executeSimple(const std::string& command, unsigned successExitCode=0);

}}

#endif /* COLIBRIMNGR_OS_HELPERS_HPP */
