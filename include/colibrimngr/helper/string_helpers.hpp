/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file string_helpers.hpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#ifndef COLIBRIMNGR_STRING_HELPERS_HPP
#define COLIBRIMNGR_STRING_HELPERS_HPP

#include <string>
#include <vector>

namespace helper {
	namespace str {

	std::vector<std::string> split(std::string const &in, char sep=' ');

	std::string join(	const std::vector<std::string>& elements, 
						const std::string& separator="", 
						const std::string& prefix="", 
						const std::string& suffix="");

	void trim_ref(std::string&);
	void rtrim_ref(std::string&);
	void ltrim_ref(std::string&);

	std::string trim(std::string);
	std::string rtrim(std::string);
	std::string ltrim(std::string);

}}

#endif /* COLIBRIMNGR_STRING_HELPERS_HPP */