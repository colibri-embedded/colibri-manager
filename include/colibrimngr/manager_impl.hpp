/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file manager_impl.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_MANAGER_IMPL_HPP
#define COLIBRIMNGR_MANAGER_IMPL_HPP

#include <chrono>
#include <colibrimngr/config.h>
#include <memory>
#include <string>
#include <vector>

#include <cache/source.hpp>
#include <handler/base.hpp>
#include <logger/base.hpp>
#include <task/common.hpp>
#include <task/task.hpp>

#define ROOT_ID 0

namespace ce {

class ManagerImpl
{
  public:
    ManagerImpl(const std::string& config_file, std::shared_ptr<logger::Base> logger, task::Callbacks callbacks = {});
    ~ManagerImpl();

    bool sync();
    bool list(const std::string& item_type, const std::string& item_arch, bool only_active = false);
    bool search(const std::vector<std::string>& item_names, const std::string& item_type, const std::string& item_arch);
    bool install(const std::vector<std::string>& item_names,
                 const std::string& item_type,
                 const std::string& item_arch,
                 bool force    = false,
                 bool is_local = false,
                 bool no_check = false);
    bool update(const std::vector<std::string>& item_names,
                const std::string& item_type,
                const std::string& item_arch,
                bool force    = false,
                bool no_check = false);
    bool remove(const std::vector<std::string>& item_names,
                const std::string& item_type,
                const std::string& item_arch,
                bool force = false);
    bool restore(const std::vector<std::string>& item_names,
                 const std::string& item_type,
                 const std::string& item_arch,
                 bool force = false);
    bool restoreFile(const std::string& file_name);
    bool activate(const std::vector<std::string>& item_names,
                  const std::string& item_type,
                  const std::string& item_arch);
    bool deactivate(const std::vector<std::string>& item_names,
                    const std::string& item_type,
                    const std::string& item_arch);

    std::vector<std::string> getItemTypes();
    std::vector<ce::Item> getItems(const std::string& item_type, const std::string& item_arch = "any");

    task::Callbacks getCallbacks();
    void setCallbacks(task::Callbacks callbacks);

    std::vector<task::sptrTask> generateSyncTasks();
    std::vector<task::sptrTask> generateTasks(const std::vector<task::Request>& requests, bool no_check = false);
    bool executeTasks(const std::vector<task::sptrTask>& tasks);

    bool onboot();

    bool finish();

    bool reload();

    std::chrono::system_clock::time_point getLastSyncTime();

    // Source control
    const std::vector<ce::cache::sptrSource>& getSources();
    bool addSource(ce::cache::sptrSource data);
    bool removeSource(const std::string& name);

  private:
    std::shared_ptr<cache::Cache> m_cache;

    task::Callbacks m_callbacks;

    std::string m_config_file;

    std::string m_bundle_info_dir;
    std::string m_bundles_dir;
    std::string m_active_bundles_dir;
    std::string m_cache_file;

    std::string m_boot_dir;
    std::string m_log_dir;
    std::string m_tmp_dir;
    std::string m_sources_dir;
    std::string m_handlers_dir;

    std::shared_ptr<logger::Base> m_logger;
    std::vector<handler::sptrBase> m_handlers;

    handler::sptrBase getHandler(const std::string& item_type, const std::string& item_arch = "*");

    void clearInstallCache();

    bool loadSettings(const std::string& config_file);
    bool loadAllRepositories();
    bool loadRepository(const std::string& file_name);
    bool loadAllExternals();

    bool isRoot();
};

} // namespace ce

#endif /* COLIBRIMNGR_MANAGER_IMPL_HPP */
