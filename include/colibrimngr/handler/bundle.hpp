/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file bundle.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_BUNDLE_HPP
#define COLIBRIMNGR_BUNDLE_HPP

#include "base.hpp"
#include <memory>
#include <regex>
#include <string>
#include <vector>

namespace ce {
namespace handler {

    class Bundle : public Base
    {
      public:
        Bundle(std::shared_ptr<logger::Base> logger);
        void setBaseDirs(const std::string& basedir, const std::string& baseinfodir, const std::string& activedir);

        bool splitFilename(std::string& fileName,
                           unsigned& priority,
                           std::string& name,
                           std::string& version) const override;

        bool install(const std::string& item_name,
                     const std::string& item_arch,
                     const std::string& item_version,
                     const std::string& item_filename,
                     bool force = false) override;
        bool remove(const std::string& item_name, const std::string& item_arch, bool force = false) override;

        bool restore(const std::string& item_name, const std::string& item_arch, bool force = false) override;

        std::shared_ptr<std::vector<Installed>> installed(const std::string& item_arch,
                                                          bool only_active = false) override;

        bool activate(const std::string& item_name, const std::string& item_arch) override;
        bool deactivate(const std::string& item_name, const std::string& item_arch) override;

        /*bool enable(std::string &item_name);
        bool disable(std::string &item_name);*/

        void onboot();

      private:
        std::regex fn_regex;

        std::string active_bundles_dir;
        std::string bundles_dir;
        std::string bundle_info_dir;
        //
        std::string getBundleFileName(const std::string& bundle_name);
        std::string getFactoryFileName(const std::string& bundle_name);
        bool isActive(const std::string& bundle_name);
        bool isInstalled(const std::string& bundle_name, std::string* bundle_file = nullptr);

        bool remountRW();
        bool remountRO();
        bool mountBundleAt(const std::string& fileName, const std::string& mountPoint);

        bool runBundleScript(const std::string& fileName,
                             const std::string& bundle_name,
                             const std::string& script_name,
                             bool active  = false,
                             bool verbose = false);
        bool runStoredBundleScript(const std::string& scriptPath, const std::string& scriptName, bool verbose = false);
        bool storeBundleScripts(const std::string& fileName,
                                const std::string& bundle_name,
                                const std::string& storePath,
                                bool active = false);

        bool removeBundle(const std::string& fileName, const std::string& bundle_name, bool postpone = false);
        void createTrigger(const std::string& fileName, const std::string& trigger_name);
    };

} /* namespace handler */
} /* namespace ce */

#endif // COLIBRIMNGR_BUNDLE_HPP
