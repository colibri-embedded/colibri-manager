/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file boot.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_BOOT_HPP
#define COLIBRIMNGR_BOOT_HPP

#include "base.hpp"
#include <memory>
#include <regex>
#include <string>
#include <vector>

namespace ce {
namespace handler {

    class Boot : public Base
    {
      public:
        Boot(std::shared_ptr<logger::Base> logger);
        void setBaseDirs(const std::string& basedir);

        bool install(const std::string& item_name,
                     const std::string& item_arch,
                     const std::string& item_version,
                     const std::string& item_filename,
                     bool force = false) override;
        std::shared_ptr<std::vector<Installed>> installed(const std::string& item_arch,
                                                          bool only_active = false) override;

        bool splitFilename(std::string& fileName,
                           unsigned& priority,
                           std::string& name,
                           std::string& version) const override;

        bool pre_install_steps(const std::vector<task::sptrTask>& tasks) const override;
        bool plan_install(std::vector<task::sptrTask>& tasks, std::shared_ptr<cache::Cache> cache) override;

      private:
        std::regex fn_regex;
        std::string boot_dir;

        bool remountRW() const;
        bool remountRO() const;
    };

} /* namespace handler */
} /* namespace ce */

#endif // COLIBRIMNGR_BOOT_HPP
