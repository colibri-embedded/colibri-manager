/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file base.
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_HANDLER_BASE_HPP
#define COLIBRIMNGR_HANDLER_BASE_HPP

#include <cache/cache.hpp>
#include <cache/item.hpp>
#include <logger/base.hpp>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include <task/task.hpp>

namespace ce {
namespace handler {

    constexpr unsigned INSTALLED = 0x01;
    constexpr unsigned ACTIVE    = 0x02;
    constexpr unsigned ONLINE    = 0x04;
    constexpr unsigned OPTIONAL  = 0x08;

    struct Installed
    {
        unsigned priority;
        unsigned state;
        std::string name;
        std::string arch;
        std::string version;

        Installed(const std::string& _name,
                  const std::string& _arch,
                  const std::string& _version,
                  unsigned _priority = 0,
                  unsigned _state    = 0)
          : name(_name)
          , arch(_arch)
          , state(_state)
          , version(_version)
          , priority(_priority)
        {}

        bool isInstalled() { return state | INSTALLED; }

        bool isActive() { return state | ACTIVE; }

        bool isOnline() { return state | ONLINE; }

        bool isOptional() { return state | OPTIONAL; }
    };

    class Base;
    typedef std::unique_ptr<Installed> uptrInstalled;
    typedef std::shared_ptr<Installed> sptrInstalled;
    typedef std::vector<uptrInstalled> uptrInstalledVector;
    typedef std::vector<sptrInstalled> sptrInstalledVector;

    class Base
    {
      public:
        explicit Base(const std::string& name, std::shared_ptr<logger::Base> logger);

        std::string name() const;

        void addSupportedCommand(const std::string& cmd);
        bool supportsCommand(const std::string& cmd) const;

        void addSupportedType(const std::string& type);
        bool supportsType(const std::string& type) const;

        void addSupportedArch(const std::string& arch);
        bool supportsArch(const std::string& arch) const;

        virtual bool isValid() const;

        virtual bool install(const std::string& item_name,
                             const std::string& item_arch,
                             const std::string& item_version,
                             const std::string& item_filename,
                             bool force = false);
        virtual std::shared_ptr<std::vector<Installed>> installed(const std::string& item_arch,
                                                                  bool only_active = false);

        virtual void clear_cached();

        virtual bool splitFilename(std::string& fileName,
                                   unsigned& priority,
                                   std::string& name,
                                   std::string& version) const;

        virtual bool remove(const std::string& item_name, const std::string& item_arch, bool force = false);
        virtual bool restore(const std::string& item_name, const std::string& item_arch, bool force = false);

        virtual bool activate(const std::string& item_name, const std::string& item_arch);
        virtual bool deactivate(const std::string& item_name, const std::string& item_arch);

        virtual bool enable(const std::string& item_name, const std::string& item_arch);
        virtual bool disable(const std::string& item_name, const std::string& item_arch);

        virtual void onboot();

        virtual bool pre_install_steps(const std::vector<task::sptrTask>& tasks) const;
        virtual bool plan_install(std::vector<task::sptrTask>& tasks, std::shared_ptr<cache::Cache> cache);

      private:
        std::string _name;
        std::set<std::string> commands;
        std::set<std::string> types;
        std::set<std::string> architectures;

      protected:
        bool valid;
        std::shared_ptr<logger::Base> output;

        std::shared_ptr<std::vector<Installed>> _installed;
    };

    typedef std::unique_ptr<Base> uptrBase;
    typedef std::shared_ptr<Base> sptrBase;

} /* namespace handler */
} /* namespace ce */

#endif // COLIBRIMNGR_HANDLER_BASE_HPP
