/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file manager.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_MANAGER_HPP
#define COLIBRIMNGR_MANAGER_HPP

#include <chrono>
#include <memory>
#include <string>
#include <vector>

#include <colibrimngr/cache/source.hpp>
#include <colibrimngr/config.h>
#include <colibrimngr/logger/base.hpp>
#include <colibrimngr/task/common.hpp>
#include <colibrimngr/task/task.hpp>

namespace ce {

class ManagerImpl;

class Manager
{
  public:
    Manager(const std::string& config_file       = MAIN_CONFIG_FILE,
            std::shared_ptr<logger::Base> logger = nullptr,
            task::Callbacks callbacks            = {});
    ~Manager();

    bool sync();
    bool list(const std::string& item_type, const std::string& item_arch, bool only_active = false);
    bool search(const std::vector<std::string>& item_names, const std::string& item_type, const std::string& item_arch);
    bool install(const std::vector<std::string>& item_names,
                 const std::string& item_type,
                 const std::string& item_arch,
                 bool force    = false,
                 bool is_local = false,
                 bool no_check = false);
    bool update(const std::vector<std::string>& item_names,
                const std::string& item_type,
                const std::string& item_arch,
                bool force    = false,
                bool no_check = false);
    bool remove(const std::vector<std::string>& item_names,
                const std::string& item_type,
                const std::string& item_arch,
                bool force = false);
    bool restore(const std::vector<std::string>& item_names,
                 const std::string& item_type,
                 const std::string& item_arch,
                 bool force = false);
    bool restoreFile(const std::string& file_name);
    bool activate(const std::vector<std::string>& item_names,
                  const std::string& item_type,
                  const std::string& item_arch);
    bool deactivate(const std::vector<std::string>& item_names,
                    const std::string& item_type,
                    const std::string& item_arch);

    std::chrono::system_clock::time_point getLastSyncTime();

    /**
     * Get available item types
     */
    std::vector<std::string> getItemTypes();

    /**
     * Get all items of specific type
     */
    std::vector<ce::Item> getItems(const std::string& item_type, const std::string& item_arch = "any");

    /**
     * Get registered callbacks
     */
    task::Callbacks getCallbacks();

    /**
     * Set callbacks
     */
    void setCallbacks(task::Callbacks callbacks);

    /**
     * Generate tasks for synchronization
     *
     * @returns List of tasks
     */
    std::vector<task::sptrTask> generateSyncTasks();

    /**
     * Generate task list based on action requests.
     *
     * @param requests List of requests
     * @param no_check Skip version check
     *
     * @returns List of tasks
     */
    std::vector<ce::task::sptrTask> generateTasks(const std::vector<ce::task::Request>& requests,
                                                  bool no_check = false);

    /**
     * Execute tasks.
     *
     * @param tasks List of tasks
     *
     * @returns true on success, false otherwise
     */
    bool executeTasks(const std::vector<ce::task::sptrTask>& tasks);

    /**
     * Get sources.
     */
    const std::vector<ce::cache::sptrSource>& getSources();

    /**
     * Add new source and store it to "source.d"
     */
    bool addSource(ce::cache::sptrSource data);

    /**
     * Remove existing source from "source.d"
     */
    bool removeSource(const std::string& name);

    bool onboot();
    bool finish();
    bool reload();

  private:
    std::unique_ptr<ManagerImpl> impl;
};

} /* namespace ce */

#endif /* COLIBRIMNGR_MANAGER_HPP */
