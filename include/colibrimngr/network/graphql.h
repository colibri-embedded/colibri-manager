/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file graphql.h
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRIMNGR_GRAPHQL_H
#define COLIBRIMNGR_GRAPHQL_H

#define QUERY_VERSIONS \
    "{{\
  latestVersions(hashId:\\\"{0}\\\"){{\
    repos {{\
      name\
      type\
      items(filter: {{branch:{{name_in:[ {1} ]}} }}) {{\
        name\
        priority\
        hashId\
        optional\
        branch {{\
          name\
        }}\
        latest {{\
          version\
          changelog\
          deps {{\
            name\
            condition\
            type\
          }}\
          files {{\
            name\
            checksum\
            algorithm\
            hashId\
          }}\
        }}\
      }}\
      repos {{\
        type\
        name\
        items(filter: {{branch:{{name_in:[ {1} ]}} }}) {{\
          name\
          priority\
          hashId\
          optional\
          branch {{\
            name\
          }}\
          latest {{\
            version\
            changelog\
            deps {{\
              name\
              condition\
              type\
            }}\
            files {{\
              name\
              checksum\
              algorithm\
              hashId\
            }}\
          }}\
        }}\
      }}\
    }}\
  }}\
}}"

#endif /* COLIBRIMNGR_GRAPHQL_H */