/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @file http_client.hpp
 * 
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 * 
 */
#ifndef COLIBRIMNGR_HTTP_CLIENT_HPP
#define COLIBRIMNGR_HTTP_CLIENT_HPP

#include <string>
#include <functional>
#include <curl/curl.h>

namespace ce {
	namespace network {

	typedef std::function<void(unsigned long total, unsigned long current)> http_progress_callback;
	typedef std::function<void()> http_finished_callback;

	class HttpClient {
	public:
		explicit HttpClient(const std::string& url = "");

		void setBaseUrl(const std::string& url);
		std::string gql_query(const std::string& suburl, const std::string& data);

		bool downloadFile(const std::string &url, const std::string &fileName);
		
		http_progress_callback progressCallback;
		http_finished_callback finishedCallback;
	private:
		std::string base_url;
	};

	} /* namespace network */
} /* namespace ce */

#endif /* COLIBRIMNGR_HTTP_CLIENT_H */
