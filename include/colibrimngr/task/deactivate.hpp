/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file deactivate.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRI_TASK_DEACTIVATE_HPP
#define COLIBRI_TASK_DEACTIVATE_HPP

#include <colibrimngr/task/task.hpp>

namespace ce {

namespace task {

    class Deactivate : public Task
    {
      public:
        Deactivate(const std::shared_ptr<handler::Base>& handler,
                   const std::string& item_name,
                   const std::string& item_type,
                   const std::string& item_arch);

        Deactivate(const std::shared_ptr<handler::Base>& handler, const task::Request& request);

        bool execute(const std::shared_ptr<logger::Base>&, const std::string& tmp_dir) override;
    };

    typedef std::shared_ptr<Deactivate> sptrDeactivate;

} // namespace task

} // namespace ce

#endif /* COLIBRI_TASK_DEACTIVATE_HPP */