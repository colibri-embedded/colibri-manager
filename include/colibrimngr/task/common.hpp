/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file common.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRI_TASK_COMMON_HPP
#define COLIBRI_TASK_COMMON_HPP

#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace ce {

namespace task {

    class Task;

    enum class Type
    {
        NONE = 0,
        CUSTOM,
        SYNC,
        DOWNLOAD,
        REMOVE,
        INSTALL,
        UPDATE,
        RESTORE,
        DEACTIVATE,
        ACTIVATE,
    };

    std::string toString(Type t);

    enum class Status
    {
        IDLE = 0,
        WORKING,
        DONE,
        FAILED,
        ABORTED
    };

    std::string toString(Status t);

    struct Item
    {
        std::string name    = "";
        std::string type    = "";
        std::string arch    = "";
        std::string version = "";
        unsigned priority   = 0;
    };

    struct Request
    {
        Type operation = Type::NONE;
        Item item;
        bool is_forced = false;
        bool is_local  = false;
    };

    typedef std::function<void(Task* task)> task_changed_cb;
    typedef std::function<void(const std::vector<std::shared_ptr<Task>>& tasks)> tasks_created_cb;
    typedef std::function<void(bool result)> tasks_finished_cb;
    typedef std::function<void(bool value)> reboot_required_cb;

    struct Callbacks
    {
        tasks_created_cb tasks_created;
        tasks_finished_cb tasks_finished;
        task_changed_cb task_changed;
        reboot_required_cb reboot_required;
    };

} // namespace task

struct Dependency
{
    std::string name;
    std::string condition;
    std::string type;
};

struct Item
{
    std::string name;
    std::string type;
    std::string arch;
    unsigned priority;
    bool optional;
    std::string installed_version;
    std::string latest_version           = "";
    std::string changelog                = "";
    std::vector<Dependency> dependencies = {};
};

} // namespace ce

#endif /* COLIBRI_TASK_COMMON_HPP */