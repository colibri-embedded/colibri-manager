/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file task.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLIBRI_TASK_HPP
#define COLIBRI_TASK_HPP

#include <colibrimngr/logger/base.hpp>
#include <colibrimngr/task/common.hpp>
#include <functional>
#include <memory>
#include <string>

namespace ce {
namespace handler {
    class Base;
}

namespace task {

    typedef std::function<bool(const std::shared_ptr<logger::Base>&, const std::string&)> task_execute_cb;

    class Task
    {
      public:
        Task(const std::shared_ptr<handler::Base>& handler,
             task::Type _type,
             const std::string& item_name,
             const std::string& item_type,
             const std::string& item_arch,
             const std::string& item_version,
             unsigned priority,
             bool local  = false,
             bool forced = false);

        Task(const std::shared_ptr<handler::Base>& handler,
             task::Type _type,
             const std::string& item_name,
             const std::string& item_type,
             const std::string& item_arch,
             bool forced = false,
             bool local  = false);

        Task(const std::shared_ptr<handler::Base>& handler, const task::Request& request);

        void setId(unsigned value);
        unsigned getId() const;

        task::Status getStatus() const;
        void setStatus(task::Status value);

        task::Type getType() const;

        const task::Item& getItem() const;

        const std::string& getFileName() const;
        void setFileName(const std::string& value);

        const std::string& getFileUrl() const;
        void setFileUrl(const std::string& value);

        bool isForced() const;
        void setForced(bool value);

        bool isLocal() const;
        void setLocal(bool value);

        unsigned long getProgress() const;
        void setProgress(unsigned long value);

        unsigned long getTotalProgress() const;
        void setTotalProgress(unsigned long value);

        std::shared_ptr<handler::Base>& getHandler();
        const std::shared_ptr<handler::Base>& getHandler() const;

        std::shared_ptr<Task> getDependee();
        void setDependee(const std::shared_ptr<Task>& value);

        void setChecksum(const std::string& value);
        const std::string& getChecksum() const;

        void setChecksumAlgorithm(unsigned value);
        unsigned getChecksumAlgorithm() const;

        bool validateFileChecksum(const std::string& filename);

        Callbacks getCallbacks();
        void setCallbacks(Callbacks callbacks);

        void setExecuteCallback(task_execute_cb callback);
        // task_execute_cb getExecuteCallback();

        virtual bool execute(const std::shared_ptr<logger::Base>&, const std::string& tmp_dir);

      protected:
        unsigned m_id;

        std::shared_ptr<handler::Base> m_handler;
        task::Type m_type;
        task::Status m_status;

        task::Item m_item;

        std::string m_filename;

        std::string m_file_url;
        std::string m_checksum;
        unsigned m_checksum_alg;

        bool m_forced;
        bool m_local;

        unsigned long m_total_progress;
        unsigned long m_current_progress;

        std::shared_ptr<Task> m_dependee;

        Callbacks m_callbacks;
        task_execute_cb m_execute;

        void task_changed();
    };

    typedef std::shared_ptr<Task> sptrTask;

} // namespace task

} // namespace ce

#endif /* COLIBRI_TASK_HPP */