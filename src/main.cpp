/**
 * Copyright (c) 2018-2019 Colibri-Embedded
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @file main.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include <CLI/CLI.hpp>
#include <iostream>
#include <manager.hpp>

#include <colibrimngr/config.h>
#include <colibrimngr/helper/os_helpers.hpp>
#include <colibrimngr/logger/json.hpp>
#include <colibrimngr/logger/none.hpp>
#include <colibrimngr/logger/stdio.hpp>
#include <colibrimngr/version.h>

#include <sys/ioctl.h> //ioctl() and TIOCGWINSZ
#include <unistd.h>    // for STDOUT_FILENO

using namespace ce::logger;

void show_version(size_t)
{
    std::cout << COLIBRI_MANAGER_PACKAGE_STRING << std::endl;
    throw CLI::Success();
}

int main(int argc, const char** argv)
{
    CLI::App app{ COLIBRI_MANAGER_PACKAGE_NAME };

    enum class Output
    {
        NONE = 0,
        STDIO,
        JSON
    };

    bool only_active        = false;
    bool force              = false;
    bool local              = false;
    bool no_check           = false;
    unsigned log_level      = LogLevel::INFO;
    Output output           = Output::STDIO;
    std::string config_file = MAIN_CONFIG_FILE;
    std::vector<std::string> item_names;
    std::string item_type = "os.bundles";
    std::string item_arch = "armhf";

    app.add_flag_function("-v,--version", show_version, "Show version");

    std::vector<std::pair<std::string, LogLevel>> lmap{
        { "critical", LogLevel::CRITICAL }, { "error", LogLevel::ERROR }, { "warn", LogLevel::WARN },
        { "info", LogLevel::INFO },         { "debug", LogLevel::DEBUG },
    };
    app.add_option("-d,--debug", log_level, "Log level (critical,error,warn,info,debug) default[info]")
        ->transform(CLI::CheckedTransformer(lmap, CLI::ignore_case));

    std::vector<std::pair<std::string, Output>> omap{ { "off", Output::NONE },
                                                      { "stdio", Output::STDIO },
                                                      { "json", Output::JSON } };
    app.add_option("-o,--output", output, "Output format (off,stdio,json) default[stdio]")
        ->transform(CLI::CheckedTransformer(omap, CLI::ignore_case));

    app.require_subcommand(/* min */ 1, /* max */ 1);
    app.fallthrough(false);
    app.add_option("-c,--config", config_file, "Configuration file");

    auto list = app.add_subcommand("list", "List command");
    list->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    list->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    list->add_flag("-a,--active", only_active, "List only active bundles");

    auto search = app.add_subcommand("search", "Search command");
    search->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    search->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    search->add_option("item_name", item_names, "Item name")->required();

    auto install = app.add_subcommand("install", "Install command");
    install->add_flag("-n,--no-check", no_check, "No version check");
    install->add_flag("-f,--force", force, "Force installation");
    install->add_flag("-l,--local", local, "Install from local file");
    install->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    install->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    install->add_option("item_name", item_names, "Item filename")->required();

    auto remove = app.add_subcommand("remove", "Remove command");
    remove->add_flag("-f,--force", force, "Force removal");
    remove->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    remove->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    remove->add_option("item_name", item_names, "Item filename")->required();

    auto update = app.add_subcommand("update", "Update command");
    update->add_flag("-n,--no-check", no_check, "No version check");
    update->add_flag("-f,--force", force, "Force update");
    update->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    update->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    update->add_option("item_name", item_names, "Item name")->required();

    auto restore = app.add_subcommand("restore", "Restore command");
    restore->add_flag("-f,--force", force, "Force restore");
    restore->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    restore->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    restore->add_option("item_name", item_names, "Item filename")->required();

    auto activate = app.add_subcommand("activate", "Activate command");
    activate->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    activate->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    activate->add_option("item_name", item_names, "Item filename")->required();

    auto deactivate = app.add_subcommand("deactivate", "Deactivate command");
    deactivate->add_option("-t,--type", item_type, "Item type [default: " + item_type + "]");
    deactivate->add_option("-m,--arch", item_arch, "Item arch [default: " + item_arch + "]");
    deactivate->add_option("item_name", item_names, "Item filename")->required();

    auto onboot = app.add_subcommand("onboot", "Execute on-boot postponed commands");

    auto sync = app.add_subcommand("sync", "Sync local cache with online repository");

    CLI11_PARSE(app, argc, argv);

    std::shared_ptr<ce::logger::Base> logger;

    switch (output) {
        case Output::NONE:
            logger = std::make_shared<ce::logger::None>(log_level);
            break;
        case Output::JSON:
            logger = std::make_shared<ce::logger::Json>(log_level);
            break;
        default:
            logger = std::make_shared<ce::logger::Stdio>(log_level);
            break;
    }

    unsigned long* old_progress;

    auto cbs = ce::task::Callbacks{
        [&old_progress](const std::vector<std::shared_ptr<ce::task::Task>>& tasks) {
            //  std::cout << "tasks.created\n";
            old_progress = new unsigned long[tasks.size() + 1];
            for (int i = 0; i < tasks.size() + 1; i++)
                old_progress[i] = 0;
        },
        [&old_progress](bool result) {
            if (result) {
                std::cout << "Finished successfully!\n";
            } else {
                std::cout << "Finished with errors!\n";
            }
            delete[] old_progress;
        },
        [&old_progress](ce::task::Task* task) {
            auto status   = task->getStatus();
            auto type     = task->getType();
            auto filename = helper::os::baseName(task->getFileName());

            int progress_bar_size = 30;
            bool skip_redraw      = true;

            float total   = task->getTotalProgress();
            float current = task->getProgress();

            int progress = (current / total) * progress_bar_size;
            if (status == ce::task::Status::DONE)
                progress = progress_bar_size;

            if (progress != old_progress[task->getId()]) {
                old_progress[task->getId()] = progress;
                skip_redraw                 = false;
            }

            if (status == ce::task::Status::DONE) {
                skip_redraw = false;
            }

            if (progress == 0)
                skip_redraw = false;

            if (skip_redraw)
                return;

            std::cout << '[';
            for (int i = 0; i < progress_bar_size; i++) {
                if (i > progress) {
                    std::cout << '-';
                } else {
                    std::cout << '#';
                }
            }
            std::cout << "] " << filename << " : " << toString(type) << '\r';

            if (status == ce::task::Status::DONE) {
                std::cout << '\n';
            }
        },
    };

    ce::Manager mngr(config_file, logger, cbs);
    bool success = false;

    if (*list) {
        success = mngr.list(item_type, item_arch, only_active);
    } else if (*search) {
        success = mngr.search(item_names, item_type, item_arch);
    } else if (*install) {
        success = mngr.install(item_names, item_type, item_arch, force, local, no_check);
    } else if (*activate) {
        success = mngr.activate(item_names, item_type, item_arch);
    } else if (*deactivate) {
        success = mngr.deactivate(item_names, item_type, item_arch);
    } else if (*remove) {
        success = mngr.remove(item_names, item_type, item_arch, force);
    } else if (*restore) {
        success = mngr.restore(item_names, item_type, item_arch, force);
    } else if (*update) {
        success = mngr.update(item_names, item_type, item_arch, force, no_check);
    } else if (*sync) {
        success = mngr.sync();
    } else if (*onboot) {
        success = mngr.onboot();
    }

    mngr.finish();

    if (!success) {
        // std::cerr << "Command failed\n";
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
