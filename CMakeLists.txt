cmake_minimum_required (VERSION 3.0)
project(colibri-manager VERSION 0.99.11)
set(PROJECT_API_VERSION 0)
# detect operating system and host processor
message(STATUS "We are on a ${CMAKE_SYSTEM_NAME} system")
message(STATUS "The host processor is ${CMAKE_HOST_SYSTEM_PROCESSOR}")

# Variables
set(INSTALL_BIN_DIR "${CMAKE_INSTALL_PREFIX}/bin" CACHE PATH "Installation directory for executables")
set(INSTALL_LIB_DIR "${CMAKE_INSTALL_PREFIX}/lib" CACHE PATH  "Installation directory for libraries")
set(INSTALL_INC_DIR "${CMAKE_INSTALL_PREFIX}/include" CACHE PATH "Installation directory for headers")
set(INSTALL_SYSCONF_DIR "${DESTDIR}/etc" CACHE PATH "The ${APPLICATION_NAME} sysconfig install dir (default prefix/etc)" FORCE)
set(INSTALL_PKGCONFIG_DIR "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" CACHE PATH "Installation directory for pkgconfig (.pc) files")
set(INSTALL_CMAKE_DIR "${CMAKE_INSTALL_PREFIX}/lib/cmake/colibrimanager" CACHE PATH  "Installation directory for libraries")
set(3RDPARTY_SOURCE_DIR "${PROJECT_SOURCE_DIR}/3rdparty")


# Skip adding rpath when linking a shared library
set (CMAKE_SKIP_RPATH NO)
# Skip adding rpath when installing the targets
set (CMAKE_SKIP_INSTALL_RPATH YES)
# Add local cmake modules
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/modules")

include(cmake/modules/InstallSymlink.cmake)

# Add project include paths
include_directories("${PROJECT_BINARY_DIR}/include")
include_directories("${PROJECT_SOURCE_DIR}/include")

# Turn off gcc 7.1 ABI incompatibility warning
add_definitions(-Wno-psabi)

# Add subdirectories
add_subdirectory(3rdparty)
add_subdirectory(include)
add_subdirectory(lib)
add_subdirectory(src)
add_subdirectory(data)
add_subdirectory(cmake)
