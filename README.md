# Cross-compile build

```
mkdir build 
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=/mnt/linux/core-os/host/usr/share/buildroot/toolchainfile.cmake ..
make
```