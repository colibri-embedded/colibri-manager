# Libraries

JSON for Modern C++ https://nlohmann.github.io/json/
CLI11: Command line parser for C++11 https://github.com/CLIUtils/CLI11
This is a header only C++ version of inih https://github.com/jtilly/inih
{fmt} is an open-source formatting library for C++. https://github.com/fmtlib/fmt
Very fast, header only, C++ logging library. https://github.com/gabime/spdlog