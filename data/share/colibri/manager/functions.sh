#!/bin/bash
########################################################################
#
#  This file is part of colibri-linux.
#  
#  Copyright (C) 2016-2018	Daniel Kesler <kesler.daniel@gmail.com>
#  
#  colibri-linux is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  colibri-linux is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################

# Load earlyboot configuration
source /mnt/live/lib/.config

TMP_MOUNT_PATH="/run/colibrimngr/tmp_mount"

R_SUCCESS=0
R_REBOOT_REQUIRED=1
R_ABORTED=2

remount_bundles_rw() {
	mount -o remount,rw /mnt/live/${BUNDLES}
}

remount_bundles_ro() {
	mount -o remount,ro /mnt/live/${BUNDLES}
}

remount_boot_rw() {
	mount -o remount,rw /mnt/live/mnt/boot
}

remount_boot_ro() {
	mount -o remount,ro /mnt/live/mnt/boot
}

##
# Mount bundle at the provided path
#
# $1 - B_FILENAME, bundle filename
# $2 - MOUNT_PATH, will be created if does not exists
#
mount_bundle_at() {
	B_FILENAME="$1"
	MOUNT_PATH="$2"
	
	mkdir -p "$MOUNT_PATH"
	mount -o loop -t squashfs "${B_FILENAME}" "${MOUNT_PATH}"
}

##
# Check if bundle version string is valid
#
# $1 - bundle version string
#
is_version_format_valid() {
	echo $1 | grep -Eq "^[0-9]+([.][0-9]+)*$"
}

##
# Extract bundle info from filename
#
# $1 - bundle filename
# $2 - info (name, version, priority, file)
filename_to_bundleinfo() {
	bundle=$(basename $1)
	B=${bundle%%.cb*}
	PRI=${B%%-*}
	N1=${B##[0-9][0-9][0-9]-}
	NAME=${N1%%-v[0-9]*}
	VER=${N1##*[a-z_0-9]-v}
	case "$2" in
		name)
			echo $NAME
			;;
		version)
			echo $VER
			;;
		priority)
			echo $PRI
			;;
		file)
			echo $bundle
			;;
		*)
			;;
	esac
}

##
#
# $1 - B_FILENAME
#
copy_to_bundles() {
	B_FILE=$(basename $1)
	if [ -f /mnt/live/${BUNDLES}/${B_FILE}.remove ]; then
		# Postpone the overwrite until reboot
		cp $1 /mnt/live/${BUNDLES}/${B_FILE}.overwrite
		chmod 644 /mnt/live/${BUNDLES}/${B_FILE}.overwrite
		cp ${1}.md5sum /mnt/live/${BUNDLES}/${B_FILE}.md5sum.overwrite
		chmod 644 /mnt/live/${BUNDLES}/${B_FILE}.md5sum.overwrite
	else
		cp $1 /mnt/live/${BUNDLES}/
		chmod 644 /mnt/live/${BUNDLES}/${B_FILE}
	fi
}

##
# 
# $1 - B_FILENAME
# $2 - TRIGGER_NAME
#
create_trigger()
{
	B_FILE=$(basename $1)
	TRIGGER=$2
	
	if [ -n $TRIGGER ]; then
		touch /mnt/live/${BUNDLES}/${B_FILE}.${TRIGGER}
		dbg_msg "TRIGGER for ${B_FILE}.${TRIGGER}"
	fi
}

copy_to_boot() {
	SRC_FILE=$1
	DST_FILE=$2
	
	if [ -f /mnt/live/mnt/boot/${DST_FILE} ]; then
		mv /mnt/live/mnt/boot/${DST_FILE} /mnt/live/mnt/boot/${DST_FILE}.backup
	fi
	
	cp ${SRC_FILE} /mnt/live/mnt/boot/${DST_FILE}
}

extract_to_boot() {
	SRC_FILE=$1
	
	unzip -o $SRC_FILE -d /mnt/live/mnt/boot/
}

##
#
# $1 - B_NAME
# $2 - Postpone (yes|no)
# $3 - New bundle filename
#
remove_bundle() {
	B_NAME="$1"
	POSTPONE="$2"
	NEW_B_FILENAME="$3"
	CAN_REMOVE="no"
	
	BUNDLE=$(is_bundle_active $1)
	if [ "x$BUNDLE" == "x" ]; then
		CAN_REMOVE="yes"
	fi

	if [ $CAN_REMOVE == "yes" ]; then
		
		# pre_remove script execute
		exec_bundle_script ${B_NAME} pre_remove ${NEW_B_FILENAME}
	
		BUNDLE=$(is_bundle_installed $B_NAME)
		if [ "x$BUNDLE" != "x" ]; then
			echo -n "Removing $BUNDLE..."
			rm -f /mnt/live/${BUNDLES}/${BUNDLE}
			rm -f /mnt/live/${BUNDLES}/${BUNDLE}.*
			echo "DONE"
		fi
		
		# post_remove script execute
		exec_bundle_script $B_NAME post_remove ${NEW_B_FILENAME}
	else
		if [ $POSTPONE == "yes" ]; then
			echo "Bundle $BUNDLE removal is postponed."
			exec_bundle_script $B_NAME postpone ${NEW_B_FILENAME}
			
			touch /mnt/live/${BUNDLES}/${BUNDLE}.remove
			create_trigger ${NEW_B_FILENAME} post_remove
			return $R_REBOOT_REQUIRED
		else
			echo "Bundle must be inactive to be removed."
			return $R_ABORTED
		fi
	fi
	
	return $R_SUCCESS
}

##
#
# $1 - B_NAME
#
activate_bundle() {
	B_NAME="$1"
	CB=$(is_bundle_active $B_NAME)
	if [ "x$CB" == "x" ]; then
		# Bundle is not already active.
		CB=$(is_bundle_installed $B_NAME)
		if [ "x$CB" != "x" ]; then
			B_FILENAME="/mnt/live/${BUNDLES}/${CB}"
			
			exec_bundle_script $B_NAME pre_activate ${B_FILENAME}
		
			# Bundle is installed so let's activate it.
			echo -n "Activating..."
			#~ mkdir -p /mnt/live/${BDATA}/${CB}
			#~ mount -o loop -t squashfs "/mnt/live/${BUNDLES}/${CB}" "/mnt/live/${BDATA}/${CB}" 2>/dev/null
			mount_bundle_at "${B_FILENAME}" "/mnt/live/${BDATA}/${CB}"
			mount -o remount,add:1:"/mnt/live/${BDATA}/${CB}" aufs / 2>/dev/null
			
			exec_bundle_script $B_NAME post_activate
			
			echo "DONE"
		else
			echo "Bundle not installed"
		fi
	fi
}

##
#
# $1 - B_NAME
# $2 - Force deactivation (yes|no)
deactivate_bundle() {
	### @TODO: implement forcefull deactivation
	### @TODO: implement 'abort' in case processes are still using the bundle's content
	B_NAME="$1"
	FORCE="$2"
	CB=$(is_bundle_active $1)
	
	
	if [ "x$CB" != "x" ]; then
		exec_bundle_script $B_NAME pre_deactivate
		
		echo -n "Deactivating..."
		mount -t aufs -o remount,verbose,del:"/mnt/live${BDATA}/${CB}" aufs / 2>/dev/null
		umount /mnt/live/${BDATA}/${CB} 2>/dev/null 
		rm -rf /mnt/live/${BDATA}/${CB}
		
		exec_bundle_script $B_NAME post_deactivate
		
		echo "DONE"
	else
		CB=$(is_bundle_installed $B_NAME)
		if [ "x$CB" == "x" ]; then
			echo "Bundle not installed"
		else
			echo "Bundle is already deactivated"
		fi
	fi
}

is_bundle_installed() {
	NEEDLE=$1
	B_DIR=/mnt/live/${BUNDLES}
	if [ "x$2" != "x" ]; then
		B_DIR=$2
	fi
	
	for bundle in $(cd ${B_DIR}; ls -d *.${BEXT});do
		B=${bundle%%.cb*}
		#PRI=${B%%-*}
		N1=${B##[0-9][0-9][0-9]-}
		NAME=${N1%%-v[0-9]*}
		VER=${N1##*[a-z_0-9]-v}
		OPTS=""

		if is_version_format_valid $VER; then
			if [ "x$NAME" == "x$NEEDLE" ]; then
				echo ${bundle}
				return 0
			fi
		fi
	done
	return 1
}

##
# $1 - B_NAME
#
is_bundle_active()
{
	NEEDLE=$1
	for bundle in $(cd /mnt/live/${BDATA}/; ls -d *.${BEXT});do
		B=${bundle%%.cb*}
		#PRI=${B%%-*}
		N1=${B##[0-9][0-9][0-9]-}
		NAME=${N1%%-v[0-9]*}
		VER=${N1##*[a-z_0-9]-v}
		OPTS=""

		if [ "x$NAME" == "x$NEEDLE" ]; then
			echo ${bundle}
			return 0
		fi
	done
	return 1
}

##
#
# $1 - B_NAME
# $2 - script name (pre_remove|post_remove|pre_install|post_install)
# $3 - Execute script from bundle file (temp mount)
#
exec_bundle_script()
{
	echo "exec_bundle_script:" $@
	B_NAME="$1"
	SCRIPT_NAME="$2"
	B_FILENAME="$3"
	CUSTOM_ROOT=""
	
	if [ -n "${B_FILENAME}" ] && [ -e "${B_FILENAME}" ]; then
		CUSTOM_ROOT=${TMP_MOUNT_PATH}
		mount_bundle_at "${B_FILENAME}" "${CUSTOM_ROOT}"
	fi
	
	SCRIPT_FILE="${CUSTOM_ROOT}/var/lib/colibri/bundle/$B_NAME/$SCRIPT_NAME.sh"
	
	if [ -x ${SCRIPT_FILE} ]; then
		${SCRIPT_FILE}
	fi
	
	if [ -n "${B_FILENAME}" ] && [ -e "${B_FILENAME}" ]; then
		umount "${CUSTOM_ROOT}"
		rm -rf "${CUSTOM_ROOT}"
	fi	
}


